var searchData=
[
  ['bas',['bas',['../classPlayer.html#a40d87df83035ac67f7cf1a1804129015',1,'Player']]],
  ['boss',['Boss',['../classBoss.html',1,'Boss'],['../classBoss.html#af287739a9fe8cb9501795656d34f3018',1,'Boss::Boss()'],['../classBoss.html#a083a04303998b4b08f9649ced86d06b0',1,'Boss::Boss(Enemy head2, Enemy leftArm2, Enemy rightArm2)']]],
  ['boss_2ecpp',['Boss.cpp',['../Boss_8cpp.html',1,'']]],
  ['boss_2eh',['Boss.h',['../Boss_8h.html',1,'']]],
  ['bougeauto',['bougeAuto',['../classBoss.html#a7ffe7090595e33d7c3616a8a4b0fb290',1,'Boss::bougeAuto()'],['../classEnemy.html#ad5c783cc3fc86cdebcd1abce8e52bb96',1,'Enemy::bougeAuto()']]],
  ['bougeboss',['bougeBoss',['../classBoss.html#aea840f353461ad4c3870d959d452d43a',1,'Boss']]],
  ['bullet',['Bullet',['../classBullet.html',1,'Bullet'],['../classBullet.html#acd7befc0bc18907cc1d871d37bbdddeb',1,'Bullet::Bullet()'],['../classBullet.html#aa0c8092c8a10435d1469b22ac8d23863',1,'Bullet::Bullet(Hitbox &amp;h, int speed, int traj)']]],
  ['bullet_2ecpp',['Bullet.cpp',['../Bullet_8cpp.html',1,'']]],
  ['bullet_2eh',['Bullet.h',['../Bullet_8h.html',1,'']]],
  ['bulletsingame',['BulletsInGame',['../classBulletsInGame.html',1,'BulletsInGame'],['../classBulletsInGame.html#a160e0e2d9978547f39e7765dd7692964',1,'BulletsInGame::BulletsInGame()']]],
  ['bulletsingame_2ecpp',['BulletsInGame.cpp',['../BulletsInGame_8cpp.html',1,'']]],
  ['bulletsingame_2eh',['BulletsInGame.h',['../BulletsInGame_8h.html',1,'']]]
];
