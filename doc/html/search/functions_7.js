var searchData=
[
  ['image',['Image',['../classImage.html#a58edd1c45b4faeb5f789b0d036d02313',1,'Image']]],
  ['isalive',['isAlive',['../classBoss.html#a41719ae1d76f406a8d88bcbdc17d4fa0',1,'Boss::isAlive()'],['../classEnemy.html#a2cce7194016cb90b9e457b3da131fd8a',1,'Enemy::isAlive()'],['../classPlayer.html#a06e38c0f57e67eb006c9d09779bd8f8f',1,'Player::isAlive()']]],
  ['isbulletout',['isBulletOut',['../classBullet.html#a1a5441cc8ddd3f63de076a03de718399',1,'Bullet']]],
  ['isdead',['isDead',['../classEnemy.html#afa5f8d70bc8673c35bb5adf102124ebf',1,'Enemy']]],
  ['ishere',['isHere',['../classBulletsInGame.html#a41a4dfcaa5055dcf585cba404feb30ee',1,'BulletsInGame::isHere()'],['../classEnemiesInGame.html#adf508d64aa533bfb0fc4f03fa7d7406f',1,'EnemiesInGame::isHere()']]],
  ['iskill',['isKill',['../classPlayer.html#a80cb88091b7f3b3618c3722a5f4b3a04',1,'Player']]],
  ['isout',['isOut',['../classBoss.html#ac42cbe633b816b8b15a4a7d1c36f6dd7',1,'Boss::isOut()'],['../classEnemy.html#a4af4037974fef8bad212de7ccf370c92',1,'Enemy::isOut()']]],
  ['isoutofscreen',['isOutOfScreen',['../classPosition.html#aca2d7b4b9944c5579a3e5bef6a23b6d9',1,'Position']]],
  ['isplayerhit',['isPlayerHit',['../classPlayer.html#a128b2b1efcf0f0b0dd796953d2bfb4d2',1,'Player']]]
];
