CC=g++
INCLUDES=-I/usr/include/SDL2
LIBS=-lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer -lm

CFLAGS=-Wall -ggdb

all: bin/maintxt bin/mainsdl bin/maintest

bin/mainsdl: obj/mainsdl.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o  obj/sdlGame.o
	$(CC) -ggdb -o bin/mainsdl obj/mainsdl.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o  obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o   obj/sdlGame.o $(LIBS)

obj/mainsdl.o: src/mainsdl.cpp  src/Enemy.h src/Hitbox.h src/BulletsInGame.h src/EnemiesInGame.h src/Enemy.h src/Bullet.h src/Hitbox.h src/Player.h src/Position.h src/Boss.h src/Game.h src/TxtGame.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/mainsdl.cpp -o obj/mainsdl.o

bin/maintxt: obj/maintxt.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o obj/winTxt.o obj/TxtGame.o
	$(CC) -ggdb -o bin/maintxt obj/maintxt.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o  obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o  obj/winTxt.o obj/TxtGame.o $(LIBS)

obj/maintxt.o: src/maintxt.cpp  src/winTxt.h src/Enemy.h src/Hitbox.h src/BulletsInGame.h src/EnemiesInGame.h src/Enemy.h src/Bullet.h src/Hitbox.h src/Player.h src/Position.h src/Boss.h src/Game.h src/TxtGame.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/maintxt.cpp -o obj/maintxt.o

bin/maintest: obj/maintest.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o
	$(CC) -ggdb -o bin/maintest obj/maintest.o obj/BulletsInGame.o obj/EnemiesInGame.o obj/Enemy.o  obj/Hitbox.o obj/Position.o obj/Bullet.o obj/Player.o obj/Boss.o obj/Game.o $(LIBS)

obj/maintest.o: src/maintest.cpp  src/Enemy.h src/Hitbox.h src/BulletsInGame.h src/EnemiesInGame.h src/Enemy.h src/Bullet.h src/Hitbox.h src/Player.h src/Position.h src/Boss.h src/Game.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/maintest.cpp -o obj/maintest.o

obj/TxtGame.o:src/TxtGame.h  src/TxtGame.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c src/TxtGame.cpp  -o obj/TxtGame.o

obj/sdlGame.o:src/sdlGame.h  src/sdlGame.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c src/sdlGame.cpp  -o obj/sdlGame.o

obj/winTxt.o:src/winTxt.h  src/winTxt.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c src/winTxt.cpp  -o obj/winTxt.o

obj/Position.o:src/Position.h src/Position.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Position.cpp -o obj/Position.o

obj/Hitbox.o:src/Hitbox.h src/Hitbox.cpp src/Position.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Hitbox.cpp -o obj/Hitbox.o

obj/Enemy.o:src/Enemy.h src/Enemy.cpp src/Position.h src/Hitbox.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Enemy.cpp -o obj/Enemy.o

obj/Bullet.o:src/Bullet.h src/Bullet.cpp src/Hitbox.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Bullet.cpp -o obj/Bullet.o

obj/BulletsInGame.o:src/BulletsInGame.h src/BulletsInGame.cpp src/Bullet.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/BulletsInGame.cpp -o obj/BulletsInGame.o

obj/EnemiesInGame.o:src/EnemiesInGame.h src/EnemiesInGame.cpp src/Enemy.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/EnemiesInGame.cpp -o obj/EnemiesInGame.o

obj/Player.o:src/Player.h src/Player.cpp src/Hitbox.h src/Bullet.h src/BulletsInGame.h src/EnemiesInGame.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Player.cpp -o obj/Player.o

obj/Boss.o:src/Boss.h src/Boss.cpp src/Player.h src/Enemy.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Boss.cpp -o obj/Boss.o

obj/Game.o:src/Game.h src/Game.cpp src/Enemy.h src/Player.h src/Bullet.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Game.cpp -o obj/Game.o

clean:
	rm obj/*.o

veryclean:
	 rm bin/*

doc:
	doxygen doc/image.doxy
