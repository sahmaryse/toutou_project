#include <cassert>
#include <time.h>
#include "sdlGame.h"
#include <stdlib.h>
#include <iostream>
#include <string.h>

using namespace std;


#define LARGEUR_TILE 24  // hauteur et largeur des tiles.
#define HAUTEUR_TILE 16

#define NOMBRE_BLOCS_LARGEUR 31  // nombre total de tile en x
#define NOMBRE_BLOCS_HAUTEUR 127
const int TAILLE_SPRITE = 31;

//CONSTRUCTION DU TERRAIN
char  table1[128][32] = {
"700000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000002200000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000034007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000034000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000220000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000222022202020222022202027",
"7000000020020202020020020202027",
"7000000020022202220020022202227",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000003400007",
"7000000002000000000000000000007",
"7000000002000000000000000000007",
"7000000002000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000002000007",
"7000000000000000000000002000007",
"7000000000000000000000002000007",
"7000340000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000022000000000000007",
"7000000000000222200000000000007",
"7000000000002222220000000000007",
"7000000000022222222000000000007",
"7000000000000022000000000000007",
"7000000000000022000000000000007",
"7000000000000022000000000000007",
"7000000000000022000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7003400000000000000000000000007",
"7000000000002000000000000000007",
"7000000000002000000000000000007",
"7000000000002000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000003400007",
"7000000000000000000000005600007",
"7000000000000000000000005600007",
"7000000022222222200000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000002000000007",
"7000000000000000000002000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000022342000000000000000007",
"7000000000560000000000000000007",
"7000000000560000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000034007",
"7000000000000000000000000002007",
"7000034000000000000000000002007",
"7000002000000000000000000002007",
"7000002000000000000000000000007",
"7000002000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000222222000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7002000000000000000000000000007",
"7002000000000000000000000000007",
"7002000000000000000000000000007",
"7002000000000000000000200000007",
"7000000000000000000000200000007",
"7000000000000000000000200000007",
"7000000000000000000000200000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000002222200007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000002000000000000000007",
"7000000000002000000000000000007",
"7000000000002000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007",
"7000000000000000000000000000007"
};



//NOM DES ITEMS DU MENU
const char * label[3]={"Commencer: Press P","Continuer: Press P","Quitter: Press Echap"};


//UN ITEM EST SELECTIONNÉ
bool selection[2]={0,0};


float temps () {
    return float(SDL_GetTicks()) / CLOCKS_PER_SEC;  // conversion des ms en secondes en divisant par 1000
}


// ============= CLASS IMAGE =============== //

Image::Image () {
    surface = NULL;
    texture = NULL;
    has_changed = false;
}

void Image::loadFromFile (const char* filename, SDL_Renderer * renderer) {
    surface = IMG_Load(filename);
    if (surface == NULL) {
        string nfn = string("../") + filename;
        cout << "Error: cannot load "<< filename <<". Trying "<<nfn<<endl;
        surface = IMG_Load(nfn.c_str());
        if (surface == NULL) {
            nfn = string("../") + nfn;
            surface = IMG_Load(nfn.c_str());
        }
    }
    if (surface == NULL) {
        cout<<"Error: cannot load "<< filename <<endl;
        exit(1);
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        cout << "Error: problem to create the texture of "<< filename<< endl;
        exit(1);
    }
}

void Image::loadFromCurrentSurface (SDL_Renderer * renderer) {
    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        cout << "Error: problem to creadessin de clé a molette the texture from surface " << endl;
        exit(1);
    }
}

void Image::draw (SDL_Renderer * renderer, int x, int y, int w, int h) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;

    if (has_changed) {
        ok = SDL_UpdateTexture(texture,NULL,surface->pixels,surface->pitch);
        assert(ok == 0);
        has_changed = false;
    }

    ok = SDL_RenderCopy(renderer,texture,NULL,&r);
    //assert(ok == 0);
}

SDL_Texture * Image::getTexture() const {return texture;}
SDL_Surface * Image::getSurface() const {return surface;}
void Image::setSurface(SDL_Surface * surf) {surface = surf;}



// ============= CLASS SDLGAME =============== //

sdlGame::sdlGame () : game()  {
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    if (TTF_Init() != 0) {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;SDL_Quit();exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_image could not initialize0666! SDL_image Error: " << IMG_GetError() << endl;SDL_Quit();exit(1);
    }

    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT,  MIX_DEFAULT_CHANNELS, 2048 ) < 0 )
    {
        cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << endl;
        cout << "No sound !!!" << endl;
        SDL_Quit();exit(1);
        withSound = false;
    }
    else withSound = true;

    //dit si il faut l'afficher le menu
    menu=true;

    //dit si le joueur bouge en utilisant le clavier
    bouge=false;

   
    //Initialisation du scroll et de la taille de la fenetre
    xscroll=0;
    yscroll= NOMBRE_BLOCS_HAUTEUR*HAUTEUR_TILE-game.DimY-1;
    game.DimX = 31;
    game.DimY = 40;
    game.DimX = game.DimX * LARGEUR_TILE;
    game.DimY = game.DimY * HAUTEUR_TILE;

    //Initialisation de la position du joueur
    Hitbox h;
    h.setXYHitbox(game.DimX/2,game.DimY-(game.DimX /LARGEUR_TILE));
    game.getPlayer().setHitboxPlayer(h);

    // Creation de la fenetre
    window = SDL_CreateWindow("Toutou project", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, game.DimX, game.DimY, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    tileset = SDL_LoadBMP("./data/tileset1.bmp");
    if (!tileset)
    {
        cout<<"Echec de chargement tileset1.bmp"<<endl;
        SDL_Quit();
    }


    //Initialisaton du terrain de jeu
    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(tileset,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(tileset);
    tileset = surfaceCorrectPixelFormat;
    texture = SDL_CreateTextureFromSurface(renderer, tileset);
    if (texture == NULL) {
        cout << "Error: problem to create the texture of "<<endl;
        exit(1);
    }


    // IMAGES
    im_player.loadFromFile("data/player.png",renderer);
    im_enemy.loadFromFile("data/enemy.png",renderer);
    im_menu.loadFromFile("data/menu.jpg",renderer);
    im_bullet.loadFromFile("data/ball.png",renderer);
    im_bulletP.loadFromFile("data/ballP.png",renderer);
    // FONTS
    font = TTF_OpenFont("data/DejaVuSansCondensed.ttf",60);
    if (font == NULL) {
            cout << "Failed to load DejaVuSansCondensed.ttf! SDL_TTF Error: " << TTF_GetError() << endl; SDL_Quit(); exit(1);
    }

    //Titre menu
    font_color.r = 0;font_color.g = 0;font_color.b = 0;
    menu_color[0].r=255;menu_color[0].g=255;menu_color[0].b=255;
    menu_color[1].r=255;menu_color[1].g=0;menu_color[1].b=0;
    font_im.setSurface(TTF_RenderText_Solid(font,"Toutou Project",font_color));
    font_im.loadFromCurrentSurface(renderer);
    //GAME OVER
    over.setSurface(TTF_RenderText_Solid(font,"GAME OVER",font_color));
    over.loadFromCurrentSurface(renderer);
    

    //Item du menu
    font_menus[0].setSurface(TTF_RenderText_Solid(font,label[0],menu_color[0]));
    font_menus[1].setSurface(TTF_RenderText_Solid(font,label[1],menu_color[0]));
    font_menus[2].setSurface(TTF_RenderText_Solid(font,label[2],menu_color[0]));
    font_menus[0].loadFromCurrentSurface(renderer);
    font_menus[1].loadFromCurrentSurface(renderer);
    font_menus[2].loadFromCurrentSurface(renderer);

    //MUSIC
     musique = Mix_LoadMUS("./data/musique.mp3");
     if (musique == NULL) {
              cout << "Failed to load musique! SDL_mixer Error: " << Mix_GetError()<< endl; SDL_Quit(); exit(1);
       }
     Mix_PlayMusic(musique, -1);

    //SON
     if (withSound)
     {
        sound = Mix_LoadWAV("./data/son.wav");
        if (sound == NULL) {
                cout << "Failed to load son.wav! SDL_mixer Error: " << Mix_GetError() << endl; SDL_Quit(); exit(1);
        }
     }
}


//DESTRUCTEUR DE LA SDLGAME
sdlGame::~sdlGame () {
    Mix_FreeMusic(musique);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

//AFFICHAGE DU FON DÉFILANT
void sdlGame::terAff(SDL_Renderer* renderer,SDL_Texture * texture)
{
    int i,j;

    Rect_source.w = LARGEUR_TILE;
    Rect_source.h = HAUTEUR_TILE;
    Rect_source.y = 0;

    Rect_dest.w = LARGEUR_TILE;
    Rect_dest.h = HAUTEUR_TILE;

    int minx,maxx,miny,maxy;
    minx = xscroll / LARGEUR_TILE-1;
    miny = yscroll / HAUTEUR_TILE-1;
    maxx = (xscroll + game.DimX)/LARGEUR_TILE;
    maxy = (yscroll + game.DimY)/HAUTEUR_TILE;

      for(i=minx;i<=maxx;i++)
       {
        for(j=miny;j<=maxy;j++)
        {
             if (i<0 || i>=NOMBRE_BLOCS_LARGEUR || j<0 || j>=NOMBRE_BLOCS_HAUTEUR)
                Rect_source.x = (table1[0][1]-'0')*LARGEUR_TILE;
            else
                Rect_source.x = (table1[j][i]-'0')*LARGEUR_TILE;

            Rect_dest.x = LARGEUR_TILE*i-xscroll;
            Rect_dest.y =j*HAUTEUR_TILE-yscroll;

            SDL_RenderCopy(renderer,texture,&Rect_source,&Rect_dest);

        }
    }

 }


void sdlGame::sdlAff ()
{
	//Remplir l'écran de blanc
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    string str2="Score : ";
    str2+=to_string(game.getPlayer().getScore());
    Score.setSurface(TTF_RenderText_Solid(font, str2.c_str(),font_color));
    Score.loadFromCurrentSurface(renderer);
    SDL_Rect rect_score;
    if (!menu){//On est dans le jeu
        terAff(renderer,texture);
	    const Player& p = game.getConstPlayer();
	    //const Enemy& e = game.getConstEnemy();
        //const Boss& boss = game.getConstBoss();
        Boss boss = game.getBoss();
        game.scroling(game.getEnemytab(),game.getTick(),game.DimX,game.DimY,1);
        game.increaseTick();
        game.actionsAutomatiques(1);
            
        if (game.getConstPlayer().getLifePlayer()==0){//Le joueur est mort
            SDL_Rect rect_over;
            rect_over.x =270;rect_over.y = 49;rect_over.w = 200;rect_over.h =200;
            SDL_RenderCopy(renderer,over.getTexture(),NULL,&rect_over);
            
            rect_score.x =350;rect_score.y =300;rect_score.w = 60;rect_score.h =60;
           SDL_RenderCopy(renderer,Score.getTexture(),NULL,&rect_score);
        } 
        else{//On est dans le jeu
            
            //La vie dun joueur
           string str1="Life : ";
           str1+=to_string(game.getPlayer().getLifePlayer());
           Life.setSurface(TTF_RenderText_Solid(font, str1.c_str(),font_color));
           Life.loadFromCurrentSurface(renderer);
          SDL_Rect rect_life;
          rect_life.x =game.DimX-90;rect_life.y =20;rect_life.w = 60;rect_life.h =60;
          SDL_RenderCopy(renderer,Life.getTexture(),NULL,&rect_life);
                 
          //
           
          rect_score.x =game.DimX-180;rect_score.y =20;rect_score.w = 60;rect_score.h =60;
          SDL_RenderCopy(renderer,Score.getTexture(),NULL,&rect_score);
	      // Afficher le sprite de Player
	      im_player.draw(renderer,p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY(),TAILLE_SPRITE,TAILLE_SPRITE);

          // Afficher le sprite du Enemy
          for ( int i=0;i<game.getEnemytab().tabsize();i++){
            im_enemy.draw(renderer,game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionX(),game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionY(),TAILLE_SPRITE,TAILLE_SPRITE);
          }
          //Afficher les balles  des Ennemies
          for ( int i=0;i<game.getBullettab().tabsize();i++){

            im_bullet.draw(renderer,game.getBullettab().returnBullet(i).getHitbox().getPositionX(),game.getBullettab().returnBullet(i).getHitbox().getPositionY(),TAILLE_SPRITE,TAILLE_SPRITE);
          }
          //Afficher les balles du joueur
         for ( int i=0;i<game.getBullettabP().tabsizePlayer();i++){
            
            im_bulletP.draw(renderer,game.getBullettabP().returnBulletPlayer(i).getHitbox().getPositionX(),game.getBullettabP().returnBulletPlayer(i).getHitbox().getPositionY(),TAILLE_SPRITE,TAILLE_SPRITE);
          }
	       // Afficher le sprite du Boss
          //tete
          //im_enemy.draw(renderer,boss.getHead().getHitboxEnemy().getPositionX()*12,boss.getHead().getHitboxEnemy().getPositionY()*12,TAILLE_SPRITE,TAILLE_SPRITE);
           //bras gauche
          //im_enemy.draw(renderer,boss.getLeftArm().getHitboxEnemy().getPositionX()*12,boss.getLeftArm().getHitboxEnemy().getPositionY()*12,TAILLE_SPRITE,TAILLE_SPRITE);
           //bras droit
	      //im_enemy.draw(renderer,boss.getRightArm().getHitboxEnemy().getPositionX()*12,boss.getRightArm().getHitboxEnemy().getPositionY()*12,TAILLE_SPRITE,TAILLE_SPRITE);

         

        }
     }
    else{//Le menu s'affiche

        im_menu.draw(renderer,0,0,40*TAILLE_SPRITE,40*TAILLE_SPRITE);

        SDL_Rect positionTitre;
        positionTitre.x = (game.DimX/4)-30;positionTitre.y =10;positionTitre.w =300;positionTitre.h =100;
        SDL_RenderCopy(renderer,font_im.getTexture(),NULL,&positionTitre);
        //Uint32 time;
        int x,y;
        SDL_Rect pos[3];
        pos[0].x=game.DimX/4;
        pos[0].y=100;
        pos[0].w=200;
        pos[0].h=30;

        pos[1].x=game.DimX/4;
        pos[1].y=150;
        pos[1].w=200;
        pos[1].h=30;

        pos[2].x=game.DimX/4;
        pos[2].y=200;
        pos[2].w=200;
        pos[2].h=30;

        for (int i=0;i<3;i++)
        {
            SDL_RenderCopy(renderer,font_menus[i].getTexture(),NULL,&pos[i]);
        }

        SDL_Event event;

            //time=SDL_GetTicks();
            while(SDL_PollEvent(&event))
           {
            switch(event.type)
            {

               case SDL_MOUSEMOTION:
                   x=event.motion.x;
                   y=event.motion.y;
                   for(int i=0;i<3;i++)
                   {
                    if (x>=pos[i].x && x<=pos[i].x+pos[i].w && y>pos[i].y && y<=pos[i].y+pos[i].h){
                         if (!selection[i])
                         {
                            selection[i]=1;
                            font_menus[i].setSurface(TTF_RenderText_Solid(font,label[i],menu_color[0]));
                            font_menus[i].loadFromCurrentSurface(renderer);
                         }
                         else
                         {
                             if (selection[i])
                              {
                                 selection[i]=0;

                                 font_menus[i].setSurface(TTF_RenderText_Solid(font,label[i],menu_color[1]));
                                  font_menus[i].loadFromCurrentSurface(renderer);
                              }
                         }
                     }
                   }

               case SDL_MOUSEBUTTONDOWN:
                  if( event.button.button == SDL_BUTTON_LEFT){
                     x=event.button.x;
                     y=event.button.y;
                      if (x>=pos[1].x && x<=pos[1].w && y>=pos[1].y && y<=pos[1].y+pos[1].h)
                      {

                        menu=false;

                      }
                    }
                    break;
            }
        }
        for (int i=0;i<3;i++)
            SDL_RenderCopy(renderer,font_menus[i].getTexture(),NULL,&pos[i]);

    }
}

void sdlGame::sdlBoucle () {
    SDL_Event events;
    //tableau pour gérer l'état des touches (clavier, souris, etc)
    int touches[324] ={0};

    bool quit = false;
    Mix_PlayMusic(musique, -1);
    // tant que ce n'est pas la fin ...
    while (!quit) {

        // tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events)) {
            switch (events.type){
               case SDL_QUIT :
                        quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
               case SDL_KEYDOWN :             // Si une touche est enfoncee
                     switch (events.key.keysym.sym) {
                     case SDLK_UP:
                         game.actionClavier('b');
                         bouge=true;
                         break;
                     case SDLK_DOWN:
                         game.actionClavier('h');
                         bouge=true;
                         break;
                     case SDLK_LEFT:
                           game.actionClavier('g');
                           bouge=true;
                         break;
                     case SDLK_RIGHT:
                           game.actionClavier('d');
                           bouge=true;
                         break;
                     case SDLK_m://Pour afficher le menu
                           menu=true;
                           Mix_PauseMusic();
                           break;
                     case SDLK_p:
                           menu=false;//Pour afficher ou revenir à l'écran de jeu
                           Mix_ResumeMusic();
                           break;
                     case SDLK_ESCAPE:
                     case SDLK_q:
                         quit = true;
                         break;
                    default: break;
                }

               case SDL_MOUSEBUTTONDOWN : //on repère quel bouton est appuyée
                       touches[events.button.button] = 1;
                        break;

               case SDL_MOUSEBUTTONUP : //on repère quel bouton est relachée
                        touches[events.button.button] = 0;
                         break;

               case SDL_MOUSEMOTION :
                     //SDL_ShowCursor(SDL_DISABLE);
                    if ( (events.motion.x > game.DimX - 1) ||
                     (events.motion.y > game.DimY - 1) ||(events.motion.y < 0) ||(events.motion.x < 0) )
                     {
                         //SDL_WarpMouse ( game.DimX-1,game.DimY-1);
                    }
                    if (touches[SDL_BUTTON_LEFT])//Le joueur se deplace avec la souris
                    {
                    Hitbox h;
                    h.setXYHitbox(events.motion.x, events.motion.y);
                    game.getPlayer().setHitboxPlayer(h);
                    }
                    break;
                    default: break;
            }
               if (withSound && bouge)
                    Mix_PlayChannel(-1,sound,0);

        }
        bouge=false;
        // on affiche le jeu sur le buffer caché

        sdlAff();
        if(!menu){//On n'est pas dans le menu donc il y scrolling
             yscroll--;
        if (xscroll<0)
            xscroll=0;
        if (yscroll<0)
            yscroll=NOMBRE_BLOCS_HAUTEUR*HAUTEUR_TILE-game.DimY-1;
        if ( yscroll> NOMBRE_BLOCS_HAUTEUR*HAUTEUR_TILE-game.DimY-1)
           yscroll= 0;
        }

        // on permute les deux buffers (cette fonction ne doit se faire qu'une seule fois dans la boucle)
        SDL_RenderPresent(renderer);
    }
}
