#ifndef WINTXT_H
#define WINTXT_H

//! \brief une fen�tre texte est un tableau 2D de caract�res
class WinTXT {

private:

    int dimx;       //!< \brief largeur
    int dimy;       //!< \brief hauteur
    char* win;      //!< \brief stocke le contenu de la fen�tre dans un tableau 1D mais on y accede en 2D

public:
    /**
    \brief Constructeur de la classe par param�tres
    */
    WinTXT (int dx, int dy);

    /**
    \brief efface
    */
    void clear (char c=' ');

    /**
    \brief dessine le caract�re en param�tre � l'�cran en position pass�e en param�tre
    */
    void print (int x, int y, char c);

    /**
    \brief dessine les caract�res en param�tres � l'�cran en position pass�e en param�tre
    */
    void print (int x, int y, char* c);

    /**
    \brief dessine la totalit� de l'�cran
    */
    void draw (int x=0, int y=0);

    /**
    \brief interrompt le processus
    */
    void pause();

    /**
    \brief renvoie un caract�re
    */
    char getCh();

};

void termClear ();

#endif
