#include"Hitbox.h"
#include"Position.h"
#include<iostream>
#include"Position.h"


Hitbox::Hitbox()
{
    Position Center;
    Radius=10;
    IsLaser=false;
}

Hitbox::Hitbox(const Position &pos,int rad,bool laser)
{
    Center=pos;
    Radius=rad;
    IsLaser=laser;
}

Hitbox::~Hitbox()
{}

Position Hitbox::getPosition()
{
    return Center;
}

int Hitbox::getPositionX()const
{
    return Center.getX();
}
int Hitbox::getPositionY()const
{
    return Center.getY();
}

int Hitbox::getRadius()const
{
    return Radius;
}

bool Hitbox::getLaser()const
{
    return IsLaser;
}

bool Hitbox::operator ==(const Hitbox & h)const
{
    if (Center==h.Center)
    {
        if (Radius==h.Radius)
        {
            if (IsLaser==h.IsLaser)
            {
                  return true;
            }
            else
            {
                return false;
            }
          }
        else
        {
            return false;
        }
    }
    else
    {
      return false;
    }
}

void Hitbox::operator =(const Hitbox h)
{
     Center=h.Center;
     Radius=h.Radius;
     IsLaser=h.IsLaser;
}

void Hitbox::setPositionX(int x)
{
    Center.setX(x);
}

void Hitbox::setPositionY(int y){

     Center.setY(y);
}

void Hitbox::setPosition(Position &p)
{
    Center = p;
}



void Hitbox::setRadius(int x)
{
    Radius=x;
}

void Hitbox::setLaser(bool a)
{
    IsLaser=a;
}

int Hitbox::getXHitbox(){
    return Center.getX();
}
int Hitbox::getYHitbox(){
    return Center.getY();
}
void Hitbox::setXYHitbox(int x, int y){
    Center.setPosition(x,y);
}
