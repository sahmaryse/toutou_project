/**
 \file Enemy.h
 \brief Permet de creer des enemies

 \version 1.0
 \date 2019/02/13
 \author
*/
#ifndef _ENEMY_H
#define _ENEMY_H

#include"Hitbox.h"
//#include"Player.h"


//! \brief Pour gérer la creation d'un  ennemi
class Enemy
{
    private:
         Hitbox hitboxEnemy; //!< \brief Hitbox de l'ennemi
         int lifeEnemy; //!< \brief vie de l'ennemi
         int patternEnemy; //!< \brief renseigne quelle option de mouvelent aura l'ennemi
         bool directionEnemy; //!< \brief direction de l'ennemi, 0 : vers le bas, 1 : vers le haut
         int tick; //!< \brief nombre de passages dans la boucle de jeu de l'ennemi
         int points; //!< \brief valeur de points qui seront ajoutés au score du joueur s'il tue l'ennemi

    public:

        /**
        \brief Constructeur de la classe
        */
        Enemy();

         /**
        \brief Constructeur de la classe
        */
        Enemy(Hitbox & h ,int l,int pattern,bool direction);

        /**
        \brief Destructeur de la classe
        */
        ~Enemy();

        /**
        \brief Verifie si l'enemy est egale a celui passer en  parametre
        */
        bool operator ==(const Enemy & e)const;

        /**
        \brief Modifie l'hitbox de l'enemy
        */
        void setHitboxEnemy(Hitbox & h);

        /**
        \brief Modifie la vie de l'enemy
        */
        void setLifeEnemy(int l);

        /**
        \brief modifie le pattern enemy
        */
        void setPatternEnemy(int pattern);

        /**
        \brief modifie la direction
        */
        void setDirectionEnemy(bool direction);

        /**
        \brief Retourne la direction de l'enemy
        */
        bool getDirectionEnemy()const;

        /**
        \brief Retourne l'hitbox de l'enemy
        */
        Hitbox getHitboxEnemy()const;

        /**
        \brief retourne la vie de l'enemy
        */
        int getLifeEnemy()const;

        /**
        \brief retourne le pattern enemy
        */
        int getPatternEnemy()const;

        /**
        \brief enleve une vie
        */
        void decreaseLife();

        /**
        \brief vérifie si l'ennemi est en vie (sa vie est nulle)
        */
        bool isAlive();

        /**
        \brief permet de générer un déplacement aléatoire d'un ennemi
        */
        void bougeAuto (int dimX, int dimY);

        /**
        \brief permet de générer un déplacement en ligne droite en allers et retours
        */
        void bougeDroit (int dimX, int dimY);

        /**
        \brief permet de générer un déplacement en diagonale vers la droite en allers et retours
        */
        void bougeDiagonaleDroite (int dimX, int dimY);

        /**
        \brief permet de générer un déplacement en diagonale vers la gauche en allers et retours
        */
        void bougeDiagonaleGauche (int dimX, int dimY);

        /**
        \brief permet de générer un déplacement en diagonale vers la gauche en allers et retours
        */
        void bougeTriangle (int dimX, int dimY);

        /**
        \brief operateur d'affectation d'ennemi par copie
        */
        void operator =(const Enemy & e);

        /**
        \brief change la position en x ET en y de l'ennemi
        */
        void setXYEnemy(int x, int y);

        /**
        \brief controle la potentielle mort de l'ennemi par collision de hitbox
        */
        bool isDead(int width, int posX);

        /**
        \brief controle la sortie d'écran de l'ennemi
        */
        bool isOut(int dimx , int dimy);

        /**
        \brief retourne le compteur de boucle
        */
        int getTick();

        /**
        \brief modifie le compteur de boucle
        */
        void setTick(int i);

        /**
        \brief retourne le nombre de points du joueur
        */

        int getPoints();

};
#endif
