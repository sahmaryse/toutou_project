#include"EnemiesInGame.h"
#include"Enemy.h"
#include<iostream>
#include<vector>
using namespace std;


EnemiesInGame::EnemiesInGame(){}

EnemiesInGame::~EnemiesInGame(){}

void EnemiesInGame::moveEnemy( BulletsInGame & tabB,int mode){
if (mode==1){
  for (long unsigned int i=0;i<tabEnemy.size();i++){
     int traj=(tabEnemy[i].getPatternEnemy());
    switch(traj)
    {

        case 1:{
              tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
              tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%100==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
              break;
        };
        case 2:{
            tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox()-1,tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%100==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
        case 3:{
            tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox()+1,tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%100==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
        case 4:{
            int corX=tabEnemy[i].getHitboxEnemy().getXHitbox();
            tabEnemy[i].setXYEnemy(corX+1,sin(corX+1)*log(corX+1));
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%100==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
    }
   }
   }
   else{
     for (long unsigned int i=0;i<tabEnemy.size();i++){
     int traj=(tabEnemy[i].getPatternEnemy());
    switch(traj)
    {

        case 1:{
              tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
              tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%1==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
              break;
        };
        case 2:{
            tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox()-1,tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%1==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
        case 3:{
            tabEnemy[i].setXYEnemy(tabEnemy[i].getHitboxEnemy().getXHitbox()+1,tabEnemy[i].getHitboxEnemy().getYHitbox()+1);
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%1==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
        case 4:{
            int corX=tabEnemy[i].getHitboxEnemy().getXHitbox();
            tabEnemy[i].setXYEnemy(corX+1,sin(corX+1)*log(corX+1));
            tabEnemy[i].setTick(tabEnemy[i].getTick()+1);
              if(tabEnemy[i].getTick()%1==0){
                Position p1(tabEnemy[i].getHitboxEnemy().getXHitbox(),tabEnemy[i].getHitboxEnemy().getYHitbox()+tabEnemy[i].getHitboxEnemy().getRadius());
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,2);
                tabB.addBullet(b1);
              }
            break;
        };
      }
    }
  }
}

void EnemiesInGame::addEnemy(Enemy & e){
  	    	tabEnemy.push_back(e);
}

void EnemiesInGame::removeEnemy(Enemy & e){
	for (unsigned int i=0;i<tabEnemy.size();i++){
		if (tabEnemy[i]==e){
		       tabEnemy.erase(tabEnemy.begin()+i);
	    }
	}
}

void EnemiesInGame::updateEnemy(int width , int posX , BulletsInGame & tabB,BulletsInGame & tabBP,int dimX,int dimY,int mode){
    moveEnemy(tabB,mode);
    for (unsigned int i=0;i<tabEnemy.size();i++){
		if (tabEnemy[i].isOut(dimX,dimY)|| tabEnemy[i].isDead(width,posX) ){
		       tabEnemy.erase(tabEnemy.begin()+i);
	    }
        for( int j=0;j<tabBP.tabsizePlayer();j++){
          /*  int inter=tabB.returnBulletPlayer(j).getHitbox().getRadius()+tabEnemy[i].getHitboxEnemy().getRadius(); 
            if(abs(tabEnemy[i].getHitboxEnemy().getXHitbox()-tabB.returnBulletPlayer(j).getXBullet())<inter&&
             abs(tabEnemy[i].getHitboxEnemy().getYHitbox()-tabB.returnBulletPlayer(j).getYBullet())<inter){
                    if(tabEnemy[i].getLifeEnemy()>0){
                        tabEnemy[i].decreaseLife();
                    }
                    else {
                        tabEnemy.erase(tabEnemy.begin()+i);
                    }*/
                int y=tabEnemy[i].getHitboxEnemy().getPositionY();// postion y enemy
                int x=tabEnemy[i].getHitboxEnemy().getPositionX();// position x enemy
                int bx=tabBP.returnBulletPlayer(j).getHitbox().getPositionX();// position balle en x
                int by=tabBP.returnBulletPlayer(j).getHitbox().getPositionY();// position balle en y
                int rb =tabBP.returnBulletPlayer(j).getHitbox().getRadius(); // radius hitbox balle
                int re =tabEnemy[i].getHitboxEnemy().getRadius();// radius hitbox enemy

                if((by-rb<y+re &&
                   by-rb>=y)&&
                   ((bx+rb>x-re&&bx+rb>x+re)||(bx-rb<x+re&&bx-rb>x-re)))
                {
                        tabEnemy.erase(tabEnemy.begin()+i);

                }

                }
        }
   
   
}

bool EnemiesInGame::isHere(int x , int y , int r){
    for (unsigned int i=0;i<tabEnemy.size();i++){
    int inter = r+tabEnemy[i].getHitboxEnemy().getRadius();

        if (abs(x-tabEnemy[i].getHitboxEnemy().getPositionX())<inter && abs(y-tabEnemy[i].getHitboxEnemy().getPositionY()<inter)){
            return true;
        }
    }
        return false;
}
/**
\brief  affichage des coordonnées des ennemies
*/
void EnemiesInGame::affichage(){
    if (tabEnemy.size()==0)
        cout<<"le tableau ne contient pas d'ennemi"<<endl;
    else
        for (unsigned int i=0;i<tabEnemy.size();i++){
            cout<<"enemy : "<<i<<endl;
            cout<<tabEnemy[i].getHitboxEnemy().getPositionX()<<endl;
            cout<<tabEnemy[i].getHitboxEnemy().getPositionY()<<endl;
    }
}

 Enemy EnemiesInGame::returnEnemy(int i){
        return tabEnemy[i];
    }

    int EnemiesInGame::tabsize(){
        return tabEnemy.size();
    }
