#ifndef _PLAYER_H
#define _PLAYER_H
#include"Hitbox.h"
#include"EnemiesInGame.h"
#include"BulletsInGame.h"
/**
 \file Player.h
 \brief Permet de creer un joueur

 \version 1.0
 \date 2019/02/13
 \author
*/

//! \brief Pour gérer la creation d'un joueur
class Player
{
private:
    int LifePlayer;
    int SpeedPlayer;
    int DamagePlayer;
    Hitbox HitboxPlayer;
    Hitbox HitboxLaser;
    int tick;
    int Score;

public:
    //creator, destructor, =
    /**
    \brief Constructeur de base
    */
    Player();
    /**
    \brief Constrcuteur avec parametre 
    */
    Player(int life,int speed, int damage,const Hitbox &hp,const Hitbox &hl);
    /**
    \brief Destructeur
    */
    ~Player();
    /**
    \brief Surchage de l'operateur =
    */
    void operator =(Player & p);

    //set
    /**
    \brief Permet de modifié la valeur de life
    */
    void setLifePlayer(int x);
    /**
    \brief Permet de modifié la valeur de score
    */
    void setScore(int x);
    /**
    \brief Permet de modifié la valeur de speed
    */
    void setSpeedPlayer(int x);
    /**
    \brief Permet de modifié la valeur de damage
    */
    void setDamagePlayer(int x);
    /**
    \brief Permet de modifié la valeur de Hitboxplayer
    */
    void setHitboxPlayer(Hitbox &h);
    /**
    \brief Permet de modifié la valeur de HitboxLaser
    */
    void setHitboxLaser(Hitbox &h);

    //get
    /**
    \brief Retourne un entier etant la valeur de  life
    */
    int getLifePlayer()const;
    /**
    \brief Retourne un entier etant la valeur de  score
    */
    int getScore()const;
    /**
    \brief Retourne un entier etant la valeur de  speed
    */
    int getSpeedPlayer()const;
    /**
    \brief Retourne un entier etant la valeur de  damage
    */
    int getDamagePlayer()const;
    /**
    \brief Retourne un entier etant la valeur de  HitboxPlayer
    */
    Hitbox getHitboxPlayer()const;
    /**
    \brief Retourne un entier etant la valeur de  HitboxLaser
    */
    Hitbox getHitboxLaser()const;

    //other functions
    /**
    \brief  Diminue la vie du joueur
    */
    void decreaseLife();
    /**
    \brief   Augmente le score
    */
    void increaseScore();
    /**
    \brief   retourne un entier permettant de savoir si le joueur est en vie
    */
    bool isAlive();
    /**
    \brief   bouge le joueur a gauche
    */
    void gauche ();
    /**
    \brief   bouge le joueur a droite
    */
    void droite ();
    /**
    \brief   bouge le joueur a haut
    */
    void haut ();
    /**
    \brief   bouge le joueur a bas
    */
    void bas ();

    //ajout victor
    /**
    \brief  Permet de deplacer le joueur
    */
    void movePlayer(int n);
    /**
    \brief  Retourne un entier si le joueur est touché
    */
    bool isPlayerHit(int x, int y);
    /**
    \brief  Met a jour joueur
    */
    void updatePlayer(EnemiesInGame & tabE, BulletsInGame & tabB);
    /**
    \brief  Verifie si le joueur est mort et retourne un bool 
    */
    bool isKill(EnemiesInGame & tabE, BulletsInGame & tabB);
    /**
    \brief  augmente le tick de player
    */
    void increaseTick();
    /**
    \brief  Retourne le tick de player
    */
    int getTick();

};

#endif
