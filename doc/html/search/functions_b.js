var searchData=
[
  ['pattern',['Pattern',['../classPattern.html#a95f42b0f1717d9e6c2d831e87d27f83c',1,'Pattern::Pattern()'],['../classPattern.html#a6296bda12a5a75cc5ec2880380ebdc0c',1,'Pattern::Pattern(int s, int st, int rf, int et, Color &amp;c)']]],
  ['pause',['pause',['../classWinTXT.html#a3e8793fd263bb51a62ec8a5e89904c49',1,'WinTXT']]],
  ['player',['Player',['../classPlayer.html#affe0cc3cb714f6deb4e62f0c0d3f1fd8',1,'Player::Player()'],['../classPlayer.html#a8d3cd9522cafe2a0650704a71637ee5a',1,'Player::Player(int life, int speed, int damage, const Hitbox &amp;hp, const Hitbox &amp;hl)']]],
  ['position',['Position',['../classPosition.html#a9097af1fd2cc15e6826e9ac2a85759bd',1,'Position::Position(int n1, int n2)'],['../classPosition.html#a369a577425f8ba02e8750d04b6a088db',1,'Position::Position()']]],
  ['print',['print',['../classWinTXT.html#a407cce45e7f81546540f4f8a9b85ce45',1,'WinTXT::print(int x, int y, char c)'],['../classWinTXT.html#ad021d5fb9862b9ea7985f8cef50451e2',1,'WinTXT::print(int x, int y, char *c)']]]
];
