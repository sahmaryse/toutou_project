#ifndef _ENEMIESINGAME_H
#define _ENEMIESINGAME_H
#include"Enemy.h"
#include"BulletsInGame.h"
#include<math.h>
#include<vector>


//! \brief Pour gérer l'arrivée et modification des ennemies du jeu
class EnemiesInGame
{
	private :
	     std::vector<Enemy> tabEnemy; //!< \brief tableau d'ennemis

    public:
    /**
 	\brief Constructeur par défaut de la classe: initialise un tableau d'ennemi
    */
	    EnemiesInGame();

	/**
  	\brief Destructeur de la classe :libère ce qui a été initialiser
  	*/
	    ~EnemiesInGame();

    /**
    \brief Permet de déplacer les ennemies et lancer leurs balles
    */
	    void moveEnemy(BulletsInGame & tabB, int mode);

    /**
  	\brief Permet d'ajouter des enemmies au tableau de enemmies
  	*/
  	void addEnemy(Enemy & e);

  	/**
  	\brief Permet de supprimer des enemies au tableau de enemies
  	*/
  	void removeEnemy(Enemy & e);

  	/**
  	\brief Permet de mettre a jour les informations d'un ennemi
  	*/
  	void updateEnemy(int width , int posX , BulletsInGame & tabB,BulletsInGame & tabBP,int Dimx,int Dimy,int mode);

  	/**
  	\brief renvoie true si un ennemi est localisé en x,y
  	*/
    bool isHere(int x , int y, int r);

    /**
  	\brief affiche un ennemi
  	*/
    void affichage();

    /**
    \brief retoune l'ennemi d'indice i
    */
    Enemy returnEnemy(int i);
    /**
    \brief retourne la taille du tableau d'ennemi
    */
    int tabsize();
};
#endif
