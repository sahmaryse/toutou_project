/**
@brief Module gérant un Game (de Player)

@file Game.h
@date 2019/03/17
*/
#ifndef _GAME_H
#define _GAME_H
#include <string>
#include <time.h>
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"
#include "BulletsInGame.h"
#include "EnemiesInGame.h"
#include "Boss.h"

/**
@brief Un jeu (de shoot them up) = un player et un ennemy
*/
class Game {

private :
    //time_t timeGame;
	Bullet b;
	Player p;
	Enemy e;
    Boss boss;
    BulletsInGame tabB;
    BulletsInGame tabBP;
    EnemiesInGame tabE;
    int tick;

public :
    int DimX;
    int DimY;
    /**
    \brief Constructeur
    */
    Game();
    /**
    \brief Retoune la reference d'une balle
    */
    Bullet& getBullets ();
    /**
    \brief Retoune la reference de player
    */
    Player& getPlayer ();
    /**
    \brief Retoune la reference vers EnemiesInGame
    */
    EnemiesInGame & getEnemytab();
    /**
    \brief Retoune la reference vers BulletInGame
    */
    BulletsInGame & getBullettab();
    /**
    \brief Retoune la reference vers BulletInGame
    */
    BulletsInGame & getBullettabP();
    /**
    \brief Retoune la reference constante de bullet
    */
    const Bullet& getConstBullets () const;
    /**
    \brief Retoune la reference constante de player
    */
    const Player& getConstPlayer () const;
    /**
    \brief Retoune la reference constante d'enemy
    */
    const Enemy& getConstEnemy () const;
    /**
    \brief Retoune la reference constante de boss
    */
    const Boss& getConstBoss () const;
    /**
    \brief Retoune boss
    */
    Boss getBoss();
    /**
    \brief Retoune le nombre d'ennemis
    */
    int getNombreEnemies() const;
    /**
    \brief Retoune un bool verifiant si la position du joueur et valide
    */
    bool estPositionPersoValide (const int x, const int y) const;

    /**
    \brief permet de faire apparaite aléatoirement des ennemis
    */
    void scroling(EnemiesInGame & tabE,int t,int dimx,int dimy,int mode);
    /**
    \brief effectue les actions automatique de la game loop
    */
    void actionsAutomatiques (int mode);
    /**
    \brief Retoune un bool si il y a une action clavier
    */
    bool actionClavier(const char touche); // rend vrai si on tire une bullet faux sinon
    /**
    \brief augmente le tick dans game de 1
    */
    void increaseTick();
    /**
    \brief Retoune le tick de game
    */
    int getTick();
};

#endif
