#include"Enemy.h"
#include"Boss.h"
#include"Bullet.h"
#include"BulletsInGame.h"
#include <stdlib.h>
#include<iostream>
#include<unistd.h>
using namespace std;

Boss::Boss()
{
    Enemy def;
    head = def;
    int xLeftArm = def.getHitboxEnemy().getPositionX() - 2;
    int xRightArm = def.getHitboxEnemy().getPositionX() + 2;
    Position posLeftArm(xLeftArm, def.getHitboxEnemy().getPositionY());
    Hitbox hitLeftArm(posLeftArm, def.getHitboxEnemy().getRadius(), def.getHitboxEnemy().getLaser());
    Position posRightArm(xRightArm, def.getHitboxEnemy().getPositionY());
    Hitbox hitRightArm(posRightArm, def.getHitboxEnemy().getRadius(), def.getHitboxEnemy().getLaser());
    leftArm.setHitboxEnemy(hitLeftArm);
    rightArm.setHitboxEnemy(hitRightArm);
}

Boss::Boss(Enemy head2, Enemy leftArm2, Enemy rightArm2)
{
    head = head2;
    leftArm = leftArm2;
    rightArm = rightArm2;
}

Boss::~Boss(){}

Enemy Boss::getHead()
{
    return head;
}

Enemy Boss::getLeftArm()
{
    return leftArm;
}

Enemy Boss::getRightArm()
{
    return rightArm;
}
/*
const Enemy& Boss::getHeadConst()
{

}

const Enemy& Boss::getLeftArmConst()
{

}

const Enemy& Boss::getRightArmConst()
{

}
*/

bool Boss::isAlive()
{
    if(not head.isAlive())
    {
        if(not leftArm.isAlive())
        {
            if(not rightArm.isAlive())
                return false;
            else return true;
        }
        else return true;
    }
    else return true;
}

bool Boss::isOut(int dimx , int dimy)
{
    //contrôle de la sortie du bras gauche
    if(leftArm.isOut(dimx,dimy))
        return true;
    else
    {
        //contrôle de la sortie du bras droit
        if(rightArm.isOut(dimx,dimy))
            return true;
        else
        {
            //controle de la sortie de la tête
            if(head.isOut(dimx,dimy))
                 return true;
            else
                return false;
        }
    }
}

void Boss::operator =(const Boss & b)
{
    head = b.head;
    leftArm = b.leftArm;
    rightArm = b.rightArm;
}

void Boss::implementeTick()
{
    head.setTick(head.getTick()+1);
    leftArm.setTick(leftArm.getTick()+1);
    rightArm.setTick(rightArm.getTick()+1);
}

void Boss::tirBoss(BulletsInGame tabB)
{
    if (head.getTick()%2==0)
    {
                Position p1(head.getHitboxEnemy().getXHitbox(), head.getHitboxEnemy().getYHitbox()+1);
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,1);
                tabB.addBullet(b1);
    }

    if (rightArm.getTick()%2==0)
    {
                Position p1(rightArm.getHitboxEnemy().getXHitbox(), rightArm.getHitboxEnemy().getYHitbox()+1);
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,1);
                tabB.addBullet(b1);
    }

    if (leftArm.getTick()%2==0)
    {
                Position p1(leftArm.getHitboxEnemy().getXHitbox(), leftArm.getHitboxEnemy().getYHitbox()+1);
                Hitbox h1(p1,1,false);
                Bullet b1(h1,2,1);
                tabB.addBullet(b1);
    }
}

void Boss::bougeBoss(int mode)
{
//TODO
}

void Boss::bougeAuto(BulletsInGame tabB,int dimX,int dimY)
{

    head.bougeDroit(dimX,dimY/3);

    Hitbox NewArm = head.getHitboxEnemy();
    int NewLeftX = NewArm.getPositionX();
    int NewLeftY = NewArm.getPositionY()-2;
    leftArm.setXYEnemy(NewLeftX,NewLeftY);

    int NewRightX = NewArm.getPositionX();
    int NewRightY = NewArm.getPositionY()+2;
    rightArm.setXYEnemy(NewRightX,NewRightY);

    implementeTick();
    tirBoss(tabB);




}
