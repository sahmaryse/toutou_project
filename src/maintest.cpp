#include "Bullet.h"
#include "Player.h"
#include "Enemy.h"
#include "EnemiesInGame.h"
#include "BulletsInGame.h"
#include "Boss.h"
#include "Game.h"
#include<iostream>
#include<vector>

using namespace std;


int main()
{
    //TEST DE POSITION

    cout<<"TEST POSITION"<<endl;
    Position pp(12,25);
    cout<<"constructeur par parametres, doit afficher 12 et 25 !"<<endl<<pp.getX()<<endl<<pp.getY()<<endl;
    Position pdef;
    cout<<"constructeur par defaut, doit afficher 0 et 0 !"<<endl<<pdef.getX()<<endl<<pdef.getY()<<endl;
    pdef.setPosition(2,2);
    cout<<"set x et y, doivent afficher 2 et 2 !"<<endl<<pdef.getX()<<endl<<pdef.getY()<<endl;
    pdef=pp;
    cout<<"operateur = , doit afficher 12 et 25"<<endl<<pdef.getX()<<endl<<pdef.getY()<<endl;
    Position pdif(13,1);

    bool btrue1 = false;
    if(pdef==pp)
        btrue1 = true;
    else
        btrue1 = false;

    bool bfalse1 = false;
    if(pdef==pdif)
        bfalse1 = true;
    else
        bfalse1 = false;
    cout<<"operateur == , doit afficher true puis false !"<<endl<<btrue1<<endl<<bfalse1<<endl;
    Position pout1(-1,1);
    Position pout2(2,98);
    Position pin(2,2);
    cout<<"fonction isOutOfScreen, doit afficher true, true, false !"<<endl<<pout1.isOutOfScreen(15,15)<<endl<<pout2.isOutOfScreen(15,15)<<endl<<pin.isOutOfScreen(15,15)<<endl;
    cout<<"POSITION OK"<<endl<<endl;

    // TEST DE HITBOX

    cout<<"TEST HITBOX"<<endl;
    Position p(12,25);
    Hitbox h1(p,15,true);
    cout<<"constructeur par parametres, doit afficher 12, 25, 15, true !"<<endl<<h1.getPositionX()<<endl<<h1.getPositionY()<<endl<<h1.getRadius()<<endl<<h1.getLaser()<<endl;
    Hitbox h2;
    cout<<"constructeur par defaut, doit afficher 0, 0, 0, false !"<<endl<<h2.getPositionX()<<endl<<h2.getPositionY()<<endl<<h2.getRadius()<<endl<<h2.getLaser()<<endl;
    h2 = h1;
    cout<<"operateur = , doit afficher 12, 25, 15, true !"<<endl<<h1.getPositionX()<<endl<<h1.getPositionY()<<endl<<h1.getRadius()<<endl<<h1.getLaser()<<endl;
    Hitbox h3;

    bool btrue2 = false;
    if(h1 == h2)
        btrue2 = true;
    else
        btrue2 = false;

    bool bfalse2 = false;
    if(h1 == h3)
        bfalse2 = true;
    else
        bfalse2 = false;
    cout<<"operateur ==, doit afficher true et false !"<<endl<<btrue2<<endl<<bfalse2<<endl;
    cout<<"HITBOX OK"<<endl<<endl;


    //TEST DE BULLET

    cout<<"TEST BULLET"<<endl;
    Bullet bp(h1,12,20);
    Hitbox bpHitbox;
    bpHitbox = bp.getHitbox();
    cout<<"constructeur par parametres, doit afficher 12, 25, 15, true, 12, 20 !"<<endl<<bpHitbox.getPositionX()<<endl<<bpHitbox.getPositionY()<<endl<<bpHitbox.getRadius()<<endl<<bpHitbox.getLaser()<<endl<<bp.getSpeedBullet()<<endl<<bp.getTrajectory()<<endl;
    cout<<"BULLET OK"<<endl<<endl;

    //TEST DE ENEMY
    
    cout<<"TEST ENEMY"<<endl;
    cout<<"A AJOUTER RESULTATS ATTENDUS"<<endl;
    Hitbox hitl(p,15,true);
    Hitbox hitp(p,12,false);
    Hitbox he(pdef,15,true);
    Enemy e(hitp,2,4,0);
    cout<< e.getHitboxEnemy().getRadius()<<" "<<e.getHitboxEnemy().getLaser()<<endl;
    cout<< e.getLifeEnemy()<<endl;
    cout<< e.getPatternEnemy()<<endl;
    e.setHitboxEnemy(hitl);
    e.setLifeEnemy(6); 
    cout<< e.getHitboxEnemy().getRadius()<<" "<<e.getHitboxEnemy().getLaser()<<endl;
    cout<< e.getLifeEnemy()<<endl;
    cout<<"ENEMY OK"<<endl<<endl;
   

    //TEST DE PLAYER
  
    cout<<"TEST PLAYER"<<endl;
    Player pdefault;
    Player pparametres(2, 2, 2, hitp, hitl);
    cout<<"constructeur par defaut, doit afficher : 0, 0, 0, 0, 0, 0, false, 0, 0, 0, true ! "<<endl<<pdefault.getLifePlayer()<<" "<<pdefault.getSpeedPlayer()<<" "<<pdefault.getDamagePlayer()<<" "<<pdefault.getHitboxPlayer().getPositionX()<<" "<<pdefault.getHitboxPlayer().getPositionY()<<" "<<pdefault.getHitboxPlayer().getRadius()<<" "<<pdefault.getHitboxPlayer().getLaser()<<" "<<pdefault.getHitboxLaser().getPositionX()<<" "<<pdefault.getHitboxLaser().getPositionY()<<" "<<pdefault.getHitboxLaser().getRadius()<<" "<<pdefault.getHitboxLaser().getLaser()<<endl;
    cout<<"constructeur par parametres, doit afficher : 2, 2, 2, 12, 25, 12, false, 12, 25, 15, true ! "<<endl<<pparametres.getLifePlayer()<<" "<<pparametres.getSpeedPlayer()<<" "<<pparametres.getDamagePlayer()<<" "<<pparametres.getHitboxPlayer().getPositionX()<<" "<<pparametres.getHitboxPlayer().getPositionY()<<" "<<pparametres.getHitboxPlayer().getRadius()<<" "<<pparametres.getHitboxPlayer().getLaser()<<" "<<pparametres.getHitboxLaser().getPositionX()<<" "<<pparametres.getHitboxLaser().getPositionY()<<" "<<pparametres.getHitboxLaser().getRadius()<<" "<<pparametres.getHitboxLaser().getLaser()<<endl;
    Player pcopy = pparametres;
    cout<<"operateur =, doit afficher : 2, 2, 2, 12, 25, 12, false, 12, 25, 15, true ! "<<endl<<pcopy.getLifePlayer()<<" "<<pcopy.getSpeedPlayer()<<" "<<pcopy.getDamagePlayer()<<" "<<pcopy.getHitboxPlayer().getPositionX()<<" "<<pcopy.getHitboxPlayer().getPositionY()<<" "<<pcopy.getHitboxPlayer().getRadius()<<" "<<pcopy.getHitboxPlayer().getLaser()<<" "<<pcopy.getHitboxLaser().getPositionX()<<" "<<pcopy.getHitboxLaser().getPositionY()<<" "<<pcopy.getHitboxLaser().getRadius()<<" "<<pcopy.getHitboxLaser().getLaser()<<endl;
    cout<<"test de decreaseLife, doit afficher 2 puis 1 !"<<endl<<pcopy.getLifePlayer();
    pcopy.decreaseLife();
    cout<<" "<<pcopy.getLifePlayer()<<endl;
    Player pdead(0, 2, 2, hitp, hitl);
    cout<<"test de isAlive, doit afficher true puis false ! "<<endl<<pcopy.isAlive()<<" "<<pdead.isAlive()<<endl;
    cout<<"PLAYER OK"<<endl<<endl;
   
    //TEST DE BULLETSINGAME
    cout<<"TEST BULLETSINGAME"<<endl;
    cout<<"A AJOUTER RESULTATS ATTENDUS"<<endl;
    BulletsInGame tabB;
    cout<<tabB.tabsize()<<" ";
    tabB.addBullet(bp);
    cout<<"New size ";
    cout<<tabB.tabsize()<<endl;
    for(int i=0;i<tabB.tabsize();i++){
        cout<<tabB.returnBullet(i).getHitbox().getPositionX()<<" "<<tabB.returnBullet(i).getHitbox().getPositionY()<<endl;
    }
    cout<<tabB.tabsize()<<" ";
    tabB.addBullet(bp);
    cout<<"New size ";
    cout<<tabB.tabsize()<<endl;
     for(int i=0;i<tabB.tabsize();i++){
         cout<<tabB.returnBullet(i).getHitbox().getPositionX()<<" "<<tabB.returnBullet(i).getHitbox().getPositionY()<<endl;
    }
    
    cout<<"BULLETSINGAME OK"<<endl<<endl;
    

    //TEST DE ENEMIESINGAME
    cout<<"TEST ENEMIESINGAME"<<endl;
    cout<<"A AJOUTER RESULTATS ATTENDUS"<<endl;
    EnemiesInGame tabE;
    cout<<tabE.tabsize()<<" ";
    tabE.addEnemy(e);
    cout<<"New size ";
    cout<<tabE.tabsize()<<endl;
    for(int i=0;i<tabE.tabsize();i++){
         cout<<tabE.returnEnemy(i).getHitboxEnemy().getXHitbox()<<" "<<tabE.returnEnemy(i).getHitboxEnemy().getYHitbox()<<endl;
    }
    cout<<tabE.tabsize()<<" ";
    tabE.addEnemy(e);
    cout<<"New size ";
    cout<<tabE.tabsize()<<endl;
    tabE.affichage();
    tabE.moveEnemy(tabB,1);
    tabE.affichage();
    Enemy e1(he,2,4,0);
    tabE.removeEnemy(e1);
    tabE.affichage();
    cout<<"ENEMIESINGAME OK"<<endl<<endl;

    

    //TEST DE BOSS
    cout<<"TEST BOSS"<<endl;
    Boss bossdefault;
    cout<<"constructeur par defaut, HEAD, doit afficher 40 0 : "<<bossdefault.getHead().getHitboxEnemy().getXHitbox()<<" "<<bossdefault.getHead().getHitboxEnemy().getYHitbox()<<endl;
    cout<<"constructeur par defaut, LEFTARM, doit afficher 30 0 : "<<bossdefault.getLeftArm().getHitboxEnemy().getXHitbox()<<" "<<bossdefault.getLeftArm().getHitboxEnemy().getYHitbox()<<endl;
    cout<<"constructeur par defaut, RIGHTARM, doit afficher 50 0 : "<<bossdefault.getRightArm().getHitboxEnemy().getXHitbox()<<" "<<bossdefault.getRightArm().getHitboxEnemy().getYHitbox()<<endl;
    Position p10(10,10);
    Position p11(11,11);
    Position p12(12,12);
    Hitbox h10(p10,0,true);
    Hitbox h11(p11,0,true);
    Hitbox h12(p12,0,true);
    Enemy e10(h10,0,0,0);
    Enemy e11(h11,0,0,0);
    Enemy e12(h12,0,0,0);
    Boss bosscopy(e10,e11,e12);
    cout<<"constructeur par copie, HEAD, doit afficher 10 10 : "<<bosscopy.getHead().getHitboxEnemy().getXHitbox()<<" "<<bosscopy.getHead().getHitboxEnemy().getYHitbox()<<endl;
    cout<<"constructeur par copie, LEFTARM, doit afficher 11 11 : "<<bosscopy.getLeftArm().getHitboxEnemy().getXHitbox()<<" "<<bosscopy.getLeftArm().getHitboxEnemy().getYHitbox()<<endl;
    cout<<"constructeur par copie, RIGHTARM, doit afficher 12 12 : "<<bosscopy.getRightArm().getHitboxEnemy().getXHitbox()<<" "<<bosscopy.getRightArm().getHitboxEnemy().getYHitbox()<<endl;
    cout<<"operateur isAlive, doit retourner true puis false : "<<bossdefault.isAlive()<<" "<<bosscopy.isAlive()<<endl;
    Hitbox hout1(pout1,0,false);
    Hitbox hout2(pout2,0,false);
    Hitbox hin(pin,0,false);
    Enemy eout1(hout1,0,0,0);
    Enemy eout2(hout2,0,0,0);
    Enemy ein(hin,0,0,0);
    Boss bossin(ein,ein,ein);
    Boss bossout1(ein,ein,eout1);
    Boss bossout2(ein,eout2,ein);
    Boss bossout3(eout2,eout2,ein);
    cout<<"sortie de terrain, doit afficher false, true, true, true : "<<bossin.isOut(50,50)<<" "<<bossout1.isOut(50,50)<<" "<<bossout2.isOut(50,50)<<" "<<bossout3.isOut(50,50)<<endl;
    bossout1 = bossin;
    cout<<"operateur d'affectation par copie (=), doit afficher false : "<<bossout1.isOut(50,50)<<endl;
    cout<<"BOSS OK"<<endl<<endl;

     cout<<"TOUTES LES CLASSES RESTANTES SONT DES CLASSES FAISANT APPEL A SDL2 ET ONT ETE TESTEES PAR AFFICHAGE"<<endl;
    //

 return 0;
}
