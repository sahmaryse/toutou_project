#include"Position.h"

Position::Position()
{
    x= 0;
    y= 0;
}

Position::Position(int n1,int n2)
{
    x=n1;
    y=n2;
}

Position::~Position()
{}

int Position::getX()const
{
        return x;
}

int Position::getY()const
{
        return y;
}

void Position::setPosition(int n1, int n2)
{
    x=n1;
    y=n2;
}
void Position::setX(int n1)
{
    x=n1;
 }

void Position::setY(int n2)
{
   y=n2;
}

void Position::operator =(const Position & p)
{
    x=p.x;
    y=p.y;
}

bool Position::operator ==(const Position & p)const
{
    if (x==p.x)
    {
        if (y==p.y)
        {
             return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
         return false;
    }
}

bool Position::isOutOfScreen(int dimX, int dimY)
{
    return(x<0 || x>dimX || y<0 || y>dimY);
}

