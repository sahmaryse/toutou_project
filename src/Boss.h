/**
 \file Boss.h
 \brief Permet de creer un boss final

 \version 1.0
 \date 2019/02/13
 \author
*/

#include"Enemy.h"
#include"Bullet.h"
#include"BulletsInGame.h"
#ifndef _BOSS_H
#define _BOSS_H

//! \brief Pour gérer le Boss de fin de jeu
class Boss
{
    private:

         Enemy head; //!< \brief partie de l'ennemi constituant sa tête
         Enemy leftArm; //!< \brief partie de l'ennemi constituant son bras gauche
         Enemy rightArm; //!< \brief bras droit

    public:

        /**
        \brief Constructeur de la classe par defaut
        */
        Boss();

         /**
        \brief Constructeur de la classe par parametres
        */
        Boss(Enemy head2, Enemy leftArm2, Enemy rightArm2);

        /**
        \brief Destructeur de la classe
        */
        ~Boss();

        /**
        \brief retourne l'enemy constituant la tête
        */
        Enemy getHead();

        /**
        \brief retourne l'enemy constituant le bras gauche
        */
        Enemy getLeftArm();

        /**
        \brief retourne l'enemy constituant le bras droit
        */
        Enemy getRightArm();

         /**
        \brief retourne l'enemy constituant la tête
        */
        const Enemy& getHeadConst();

        /**
        \brief retourne l'enemy constituant le bras gauche
        */
        const Enemy& getLeftArmConst();

        /**
        \brief retourne l'enemy constituant le bras droit
        */
        const Enemy& getRightArmConst();
        /**
        \brief vérifie si l'ennemi est en vie toutes ses vies sont nulles)
        */
        bool isAlive();

        /**
        \brief permet de choisir le mode de déplacement du boss
        En 1 : Les bras et la tête se déplacent de façon synchrone
        2 : mouvement désynchronisé des bras et de la tête
        3 : mouvement désnchronisé des parties
        */
        void bougeBoss(int mode);

        /**
        \brief permet de générer un déplacement aléatoire du boss
        */
        void bougeAuto(BulletsInGame tabB,int dimX,int dimY);

        /**
        \brief operateur d'affectation de boss par copie
        */
        void operator =(const Boss & b);

        /**
        \brief controle la sortie d'écran du boss
        */
        bool isOut(int dimx , int dimy);

        /**
        \brief implemente les ticks des enemy composant le boss
        */
        void implementeTick();

        /**
        \brief implemente les tirs des enemy composant le boss
        */
        void tirBoss(BulletsInGame tabB);

};
#endif
