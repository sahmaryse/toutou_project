/**
 \file Hitbox.h
 \brief Permet de créer des hitbox

 \version 1.0
 \date 2019/02/13
 \author
*/

#ifndef _HITBOX_H
#define _HITBOX_H

#include"Position.h"
#include<iostream>


//! \brief Pour gérer la creation sont d'une hitbox 
class Hitbox
{
    private:
        Position Center; //!< \brief Position de la hitbox
        int Radius; //!< \brief rayon de la hitbox
        bool IsLaser; //!< \brief indique si la hitbox est celle d'un laser

    public:
        /**
        \brief Constructeur de la classe par défaut
        */
        Hitbox();

        /**
        \brief Destructeur de la classe
        */
        ~Hitbox();

        /**
        \brief Constructeur de la classe par paramètres
        */
        Hitbox(const Position &p,int x ,bool y);

        /**
        \brief Operateur de comparaison
        */
        bool operator ==(const Hitbox & h)const;

        /**
        \brief Operateur d'affectation
        */
        void  operator =(const Hitbox);

        /**
        \brief affecte une position
        */
        void setPosition(Position &p);

        /**
        \brief affecte une ordonnée
        */
        void setPositionX(int x);

        /**
        \brief affecte une abscisse
        */
        void setPositionY(int y);

        /**
        \brief affecte une abscisse et une ordonnée
        */
        void setXYHitbox(int x, int y);

        /**
        \brief affecte un diametre
        */
        void setRadius(int x);

        /**
        \brief affecte un laser
        */
        void setLaser(bool a);

        /**
        \brief renvoie une position
        */
        Position getPosition();

        /**
        \brief renvoie une ordonnée constante
        */
        int getPositionX()const;

        /**
        \brief renvoie une abscisse constante
        */
        int getPositionY()const;

        /**
        \brief renvoie une ordonnée
        */
        int getXHitbox();

        /**
        \brief renvoie une abscisse
        */
        int getYHitbox();

        /**
        \brief renvoie un diamètre
        */
        int getRadius()const;

        /**
        \brief renvoie un laser
        */
        bool getLaser()const;
};
#endif
