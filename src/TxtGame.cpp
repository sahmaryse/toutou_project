#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"
#include <stdio.h>

#include <stdlib.h>
#include "Game.h"
#include "Player.h"
#include "Enemy.h"
#include "Hitbox.h"
#include "Boss.h"

void txtAff(WinTXT & win, Game & game) {
	
	const Player& p = game.getConstPlayer();
	  win.clear();
 		for(int y=0;y<game.DimY;++y){
            int x=0;
			win.print(x,y,'#');
        }
		for(int y=0;y<game.DimY;++y){
			int x=game.DimX-1;
			win.print(x,y,'#');
		}
     // Affichage de Player
	win.print(p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY(),'P');
	//win.print(p.getHitboxLaser().getPositionX(),p.getHitboxLaser().getPositionY(),'L');
	// Affichage de l'enemy
	for ( int i=0;i<game.getEnemytab().tabsize();i++){
		win.print(game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionX(),game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionY(),'E');
			if (game.getPlayer().getHitboxPlayer().getPositionX()==game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionX() &&
                game.getPlayer().getHitboxPlayer().getPositionY()==game.getEnemytab().returnEnemy(i).getHitboxEnemy().getPositionY()){
                win.clear();
                win.print(5,5,'G');
                win.print(6,5,'A');
                win.print(7,5,'M');
                win.print(8,5,'E');
                win.print(9,5,' ');
                win.print(10,5,'O');
                win.print(11,5,'V');
                win.print(12,5,'E');
                win.print(13,5,'R');
                win.draw();
                //sleep(5);
                exit(0);
            }
    }
    
     for ( int i=0;i<game.getBullettab().tabsize();i++){
			win.print(game.getBullettab().returnBullet(i).getXBullet(),game.getBullettab().returnBullet(i).getYBullet(),'B');
			if (game.getPlayer().getHitboxPlayer().getPositionX()==game.getBullettab().returnBullet(i).getXBullet() &&
                game.getPlayer().getHitboxPlayer().getPositionY()==game.getBullettab().returnBullet(i).getYBullet()){
                win.clear();
                win.print(5,5,'G');
                win.print(6,5,'A');
                win.print(7,5,'M');
                win.print(8,5,'E');
                win.print(9,5,' ');
                win.print(10,5,'O');
                win.print(11,5,'V');
                win.print(12,5,'E');
                win.print(13,5,'R');
                win.draw();
                //sleep(5);
                exit(0);
            }

    }

    for ( int i=0;i<game.getBullettabP().tabsizePlayer();i++){
        win.print(game.getBullettabP().returnBulletPlayer(i).getXBullet(),game.getBullettabP().returnBulletPlayer(i).getYBullet()-1,'W');

    }
	
	win.draw();
}

void txtBoucle (Game& game) {
	// Creation d'une nouvelle fenetre en mode texte
	// => fenetre de dimension et position (WIDTH,HEIGHT,STARTX,STARTY)
     WinTXT win (game.DimX,game.DimY);

	  bool ok = true;
	  int c;
    //  clock_t temps;

	do {
	    txtAff(win,game);
        game.scroling(game.getEnemytab(),game.getTick(),game.DimX,game.DimY,0);
        game.increaseTick();
        #ifdef _WIN32
        Sleep(100);
		#else
		usleep(100000);
        #endif // WIN32
        usleep(100000);
		game.actionsAutomatiques(0);

		c = win.getCh();
	switch (c) {
	     	case 'k':
				game.actionClavier('g');
				break;
			case 'm':
				game.actionClavier('d');
				break;
			case 'o':
				game.actionClavier('b');
				break;
			case 'l':
				game.actionClavier('h');
				break;
               
				break;
			case 'q':
				ok = false;
				break;
		}
	} while (ok);

}
