#include<time.h>
#include<unistd.h>
#include "Game.h"
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Boss.h"

Game::Game (){
    DimY=20;  
    DimX=40;
    tick=0;

}

Player& Game::getPlayer () {return p; }

EnemiesInGame & Game::getEnemytab(){  return tabE; }

BulletsInGame & Game::getBullettab(){ return tabB; }

BulletsInGame & Game::getBullettabP(){ return tabBP; }

const Bullet& Game::getConstBullets () const { return b; }

const Player& Game::getConstPlayer () const { return p; }

const Enemy& Game::getConstEnemy () const { return e; }

const Boss& Game::getConstBoss () const { return boss; }

Boss Game::getBoss () { return boss; }

bool Game::estPositionPersoValide (const int x, const int y) const 
{
	return ((x>=0) && (x<DimX) && (y>=0) && (y<DimY) );
}

int Game::getNombreEnemies() const { return 1; }


bool Game::actionClavier (const char touche) 
{
	switch(touche) {
		case 'g' :
            if (estPositionPersoValide(p.getHitboxPlayer().getPositionX()-1,p.getHitboxPlayer().getPositionY())){
				p.gauche();
                          }
				break;
		case 'd' :
		      if (estPositionPersoValide(p.getHitboxPlayer().getPositionX()+1,p.getHitboxPlayer().getPositionY())){
                  p.droite();
                          }
				break;
		case 'h' :
               if (estPositionPersoValide(p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY()+1)){
				p.haut();
                          }
				break;
		case 'b' :
               if (estPositionPersoValide(p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY()-1)){
				p.bas();
                          }
				break;
	}
	return false;
}

void Game::scroling(EnemiesInGame & tabE,int t,int dimx,int dimy,int mode){
   if (mode==1){
          if(t%642==0){
             int E = rand() % 2 + 1;
             for(int i = 0;i<E ;i++){
                Position pr(rand() % dimx + 1,1);
                Hitbox hr(pr,1,false);
                Enemy er(hr,rand() % 1 + 1,rand() % 1 + 1,0);
                tabE.addEnemy(er);
             }
           }
        }
   else{
       if(t%2==0){
        int E = rand() % 2 + 1;
        for(int i = 0;i<E ;i++){
            Position pr(rand() % dimx + 1,1);
            Hitbox hr(pr,1,false);
            Enemy er(hr,rand() % 1 + 1,rand() % 1 + 1,0);
            tabE.addEnemy(er);
        }
   }
 }
}

void Game::actionsAutomatiques (int mode) 
{
      if (mode==1){
	       tabE.updateEnemy(p.getHitboxLaser().getRadius(),p.getHitboxLaser().getPositionX(),tabB,tabBP,DimX,DimY,1);
	       tabB.updateBullets(DimX,DimY);
	       tabBP.updateBulletsPlayer(DimX,DimY);
	       p.updatePlayer(tabE,tabB);
           if(p.getTick()%100==0){
               Position p1(p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY());
               Hitbox h1(p1,1,false);
               Bullet b1(h1,1,1);
               tabBP.addBulletPlayer(b1);
          }
      }
       else{
           tabE.updateEnemy(p.getHitboxLaser().getRadius(),p.getHitboxLaser().getPositionX(),tabB,tabBP,DimX,DimY,0);
	       tabB.updateBullets(DimX,DimY);
	       tabBP.updateBulletsPlayer(DimX,DimY);
	       p.updatePlayer(tabE,tabB);
           if(p.getTick()%1==0){
              Position p1(p.getHitboxPlayer().getPositionX(),p.getHitboxPlayer().getPositionY());
              Hitbox h1(p1,1,false);
              Bullet b1(h1,1,1);
              tabBP.addBulletPlayer(b1);
           }
       }
         p.increaseTick();
}
void Game::increaseTick(){
    tick++;
}

int Game::getTick(){
    return tick;
}		

