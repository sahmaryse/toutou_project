#ifndef _POSITION_H
#define _POSITION_H

//! \brief Pour gérer la position des joueurs, balles et ennemies
class Position
{
private:
    int x,y;  //!< \brief abscisse et ordonnée de l'objet

public :
    /**
    \brief Constructeur de la classe par défaut
    */
    Position();

    /**
    \brief Constructeur de la classe par paramètres
    */
    Position(int n1,int n2);

    /**
    \brief Destructeur de la classe
    */
    ~Position();

    /**
    \brief renvoie l'abscisse de la position
    */
    int getX()const;

    /**
    \brief renvoie l'ordonnée de la position
    */
    int getY()const;

    /**
    \brief affecte une abscisse
    */
    void setX(int n1);

    /**
    \brief affecte une ordonnée
    */
    void setY(int n2);

    /**
    \brief affecte une abscisse et une ordonnée
    */
    void setPosition(int x, int y);

    /**
    \brief Constructeur de la classe par copie
    */
    void operator =(const Position & p);

    /**
    \brief operateur de comparaison
    */
    bool operator ==(const Position & p)const;

    /**
    \brief regarde si une position est hors de l'écran dont la dimension est passée en paramètre
    */
    bool isOutOfScreen(int dimX, int DimY);
};
#endif
