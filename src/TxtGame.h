/**
 \file TxtGame.h
 \brief Affichage mode txt

 \version 1.0
 \date 2019/02/13
 \author
*/
#ifndef _TXTGAME_H
#define _TXTGAME_H

#include "Game.h"

void txtBoucle (Game & game);

#endif
