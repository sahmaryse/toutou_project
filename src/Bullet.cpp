#include"Bullet.h"
#include"Hitbox.h"


Bullet::Bullet()
{
    Hitbox hdef;
    HitboxBullet = hdef;
    SpeedBullet = 1;
    Trajectory = 0;
    damage=1;
}

Bullet::Bullet(Hitbox &h, int speed, int traj)
{
    HitboxBullet=h;
    SpeedBullet=speed;
    Trajectory=traj;
}

Bullet::~Bullet()
{}

void Bullet::setHitbox(Hitbox &h)
{
    HitboxBullet=h;
}

void Bullet::setSpeedBullet(int x)
{
    SpeedBullet=x;
}

void Bullet::setTrajectory(int x)
{
    Trajectory=x;
}

Hitbox Bullet::getHitbox()
{
    return HitboxBullet;
}

int Bullet::getSpeedBullet()
{
    return SpeedBullet;
}

int Bullet::getTrajectory()
{
    return Trajectory;
}

bool Bullet::operator==(Bullet & b){
    return (HitboxBullet==b.HitboxBullet && SpeedBullet==b.SpeedBullet && Trajectory==b.Trajectory);
}
int Bullet::getXBullet(){
    return HitboxBullet.getXHitbox();
}
int Bullet::getYBullet(){
    return HitboxBullet.getYHitbox();
}
void Bullet::setXYBullet(int x , int y){
    HitboxBullet.setXYHitbox(x,y);
}

bool Bullet::isBulletOut(int dimx, int dimy){
    return (HitboxBullet.getXHitbox()<0 ||HitboxBullet.getXHitbox()>dimx || HitboxBullet.getYHitbox()<0 || HitboxBullet.getYHitbox()>dimy);
}

int Bullet::getTick(){
    return Tick;
}

void Bullet::setTick(int i){
    Tick+=i;
}

int Bullet::getDamage(){
    return damage;
}
