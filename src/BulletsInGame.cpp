#include"BulletsInGame.h"
#include"Bullet.h"
#include<vector>
using namespace std;


BulletsInGame::BulletsInGame(){}

BulletsInGame::~BulletsInGame(){}

void BulletsInGame::moveBullets(){
 for (long unsigned int i =0;i<tabBullets.size();i++){
    int traj= tabBullets[i].getTrajectory();
    switch(traj)
    {
        case 1:{
              tabBullets[i].setXYBullet(tabBullets[i].getHitbox().getXHitbox(),tabBullets[i].getHitbox().getYHitbox()+(1*tabBullets[i].getSpeedBullet()));
              break;
        }
        case 2:{
              tabBullets[i].setXYBullet(tabBullets[i].getHitbox().getXHitbox()+1,tabBullets[i].getHitbox().getYHitbox()+(1*tabBullets[i].getSpeedBullet()));
              break;
        }
        case 10:{
            if(tabBullets[i].getTick()%2==0){
                tabBullets[i].setXYBullet(tabBullets[i].getHitbox().getXHitbox(),tabBullets[i].getHitbox().getYHitbox()+(3*tabBullets[i].getSpeedBullet()));
                
            }
            else{
                tabBullets[i].setXYBullet(tabBullets[i].getHitbox().getXHitbox(),tabBullets[i].getHitbox().getYHitbox()+(1*tabBullets[i].getSpeedBullet()));
            }
            break;    

        }
    }
 }
}
void BulletsInGame::moveBulletsPlayer(){
 for (long unsigned int i =0;i<tabBulletsPlayer.size();i++){
    int traj= tabBulletsPlayer[i].getTrajectory();
    switch(traj)
    {
        case 1:{
              tabBulletsPlayer[i].setXYBullet(tabBulletsPlayer[i].getHitbox().getXHitbox(),tabBulletsPlayer[i].getHitbox().getYHitbox()-(1*tabBulletsPlayer[i].getSpeedBullet()));
              break;
        }

    }
 }
}

void BulletsInGame::addBullet(Bullet & b){
	tabBullets.push_back(b);
}


void BulletsInGame::addBulletPlayer(Bullet & b){
    tabBulletsPlayer.push_back(b);
}

void BulletsInGame::removeBullets(Bullet & b){
	for (unsigned int i=0;i<tabBullets.size();i++){
		if (tabBullets[i]==b){
		       tabBullets.erase(tabBullets.begin()+i);
	    }
	}
}

void BulletsInGame::removeBulletsPlayer(Bullet & b){
    for (unsigned int i=0;i<tabBulletsPlayer.size();i++){
        if (tabBulletsPlayer[i]==b){
               tabBulletsPlayer.erase(tabBulletsPlayer.begin()+i);
        }
    }
}

void BulletsInGame::updateBullets(int Dimx,int Dimy){
    moveBullets();
    for (unsigned int i=0;i<tabBullets.size();i++){
        if (tabBullets[i].isBulletOut(Dimx,Dimy)){
               removeBullets(tabBullets[i]);
        }
    }
}

void BulletsInGame::updateBulletsPlayer(int Dimx,int Dimy){
    moveBulletsPlayer();
    for (unsigned int i=0;i<tabBulletsPlayer.size();i++){
        if (tabBulletsPlayer[i].isBulletOut(Dimx,Dimy)){
               removeBulletsPlayer(tabBulletsPlayer[i]);
        }
    }
}

bool BulletsInGame::isHere(int x , int y, int r){
    for (unsigned int i=0;i<tabBullets.size();i++){
    int inter=r+tabBullets[i].getHitbox().getRadius();
        if (abs(x-tabBullets[i].getXBullet())<inter  && abs(x-tabBullets[i].getYBullet())<inter ){
            return true;
        }
    }
        return false;
}

void BulletsInGame::affichage(){
    if (tabBullets.size()==0)
        cout<<"le tableau ne contient pas de balle"<<endl;
    else
        for (unsigned int i=0;i<tabBullets.size();i++){
            cout<<"balle numéro : "<<i<<endl;
            cout<<tabBullets[i].getXBullet()<<endl;
            cout<<tabBullets[i].getYBullet()<<endl;
    }
}

Bullet BulletsInGame::returnBullet(int i){
        return tabBullets[i];
}

int BulletsInGame::tabsize(){
        return tabBullets.size();
}

 Bullet BulletsInGame::returnBulletPlayer(int i){
        return tabBulletsPlayer[i];
}

int BulletsInGame::tabsizePlayer(){
        return tabBulletsPlayer.size();
}