#ifndef _BULLET_H
#define _BULLET_H
#include"Hitbox.h"

//! \brief Pour gérer la creation des balles
class Bullet
{
    private:
        Hitbox HitboxBullet;  //!< \brief Hitbox de la balle
        int SpeedBullet;  //!< \brief vitesse de déplacement de la balle
        int Trajectory;  //!< \brief permet de prévoir la trajectoire que suivra la balle
        int Tick;  //!< \brief nombre de tours de jeu dans lesquels la balle a existé
        int damage;  //!< \brief nombre de dégats infligés par la balle

    public:
        /**
        \brief Constructeur de la classe par défaut
        */
        Bullet();

        /**
        \brief Constructeur de la classe par paramètres
        */
        Bullet(Hitbox &h, int speed, int traj);

        /**
        \brief Destructeur de la classe
        */
        ~Bullet();

        /**
        \brief affecte une hitbox
        */
        void setHitbox(Hitbox &h);

        /**
        \brief affecte une vitesse
        */
        void setSpeedBullet(int s);

        /**
        \brief affecte un type de trajectoire
        */
        void setTrajectory(int traj);

        /**
        \brief renvoie la hitbox
        */
        Hitbox getHitbox();

        /**
        \brief renvoie la vitesse
        */
        int getSpeedBullet();

        /**
        \brief renvoie la trajectoire
        */
        int getTrajectory();

        /**
        \brief operateur de comparaison
        */
        bool operator==(Bullet & b);

        /**
        \brief renvoie la position en x
        */
        int getXBullet();

        /**
        \brief renvoie la position en y
        */
        int getYBullet();

        /**
        \brief affecte la position en x ET en y
        */
        void setXYBullet(int x , int y);

        /**
        \brief controle la sortie de l'écran
        */
        bool isBulletOut(int dimx, int dimy);

         /**
        \brief Retourne le tick
        */
        int getTick();

         /**
        \brief Affecteur de tick
        */
        void setTick(int i);

         /**
        \brief Renvoie les dommages causés par la balle
        */
        int getDamage();
};

#endif
