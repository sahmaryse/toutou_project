/**
 \file BulletsInGame.h
 \brief Permet d'ajouter ou de supprimer des bullets

 \version 1.0
 \date 2019/02/13
 \author
*/
#ifndef _BULLETSINGAME_H
#define _BULLETSINGAME_H
#include<vector>
#include"Bullet.h"

//! \brief Pour gérer la modification des balles des ennemies et des balles du joueur
class BulletsInGame{
  private:
        std::vector<Bullet> tabBullets;
        std::vector<Bullet> tabBulletsPlayer;
  public:
  	/**

 	\brief Constructeur par défaut de la classe: initialise un tableau de bullets

    */
  	BulletsInGame();

  	/**
  	\brief Destructeur de la classe :libère e qui a été initialiser
  	*/
    ~BulletsInGame();

    /**
    \brief Permet de déplacer les bullets
    */
  	void moveBullets();

  	/**
  	\brief Permet d'ajouter des  bullets au tableau de bullets pour les ennemies
  	*/
  	void addBullet(Bullet & b);

    /**
    \brief Permet d'ajouter des  bullets au tableau de bullets pour le joueur
    */
    void addBulletPlayer(Bullet & b);


  	/**
  	\brief Permet de supprimer des bullets au tableau de bullets
  	*/
  	void removeBullets(Bullet & b);
    
    
    /**
    \brief Permet l'avvancement des balles lancer
    */
  	void updateBullets(int Dimx,int Dimy);
    void updateBulletsPlayer(int Dimx,int Dimy);
    /**
    \brief Pemet de savoir si une balle est a la position p(x,y)
    */
  	bool isHere(int x , int y, int r);

    /**
    \brief Permet d'afficher le tableau de bullets
    */
  	void affichage();
    
    /**
    \brief Retourne la  bullet à la position "i" du tableau de bullets
    */
  	Bullet returnBullet(int i);
 
    /**
    \brief Retoune la taille du tableau de bullets
    */
  	int tabsize();

     /**
    \brief Retourne la  bullet à la position "i" du tableau de bullets dujoueur
    */
    Bullet returnBulletPlayer(int i);
    
    /**
    \brief Retoune la taille du tableau de bullets du joueur
    */
    int tabsizePlayer();

    /**
    \brief Permet de se faire déplacer les balles du joueur
    */
    void moveBulletsPlayer();

    /**
    \brief Permet de supprimer une balle du tableau de balle du joueur
    */
    void removeBulletsPlayer(Bullet & b);



    
};
#endif
