#include <stdlib.h>
 #include"Player.h"
#include "Bullet.h"
#include "Hitbox.h"
//creator, destructor, =

Player::Player()
{
    LifePlayer=3;
    SpeedPlayer=1;
    DamagePlayer=0;
    Position p(10,18);
    Score=0;
    HitboxPlayer.setPosition(p);

    Position pl;
    Hitbox hl(pl,0,true);
    HitboxLaser = hl;
}

Player::Player(int life,int speed, int damage, const Hitbox &hp,const Hitbox &hl)
{
    LifePlayer=life;
    SpeedPlayer=speed;
    DamagePlayer=damage;
    HitboxPlayer=hp;
    HitboxLaser=hl;
}

Player::~Player()
{}

void Player::operator =(Player & p)
{
    LifePlayer = p.getLifePlayer();
    SpeedPlayer = p.getSpeedPlayer();
    DamagePlayer = p.getDamagePlayer();
    HitboxPlayer = p.getHitboxPlayer();
    HitboxLaser = p.getHitboxLaser();
}

//SET

void Player::setLifePlayer(int x)
{
        LifePlayer=x;
}

void Player::setScore(int x)
{
        Score=x;
}

void Player::setSpeedPlayer(int x)
{
        SpeedPlayer=x;
}

void  Player::setDamagePlayer(int x)
{
        DamagePlayer=x;
}

void Player::setHitboxPlayer(Hitbox &h)
{
        HitboxPlayer=h;
}

void Player::setHitboxLaser(Hitbox &h)
{
        HitboxLaser=h;
}

//GET

int Player::getLifePlayer()const
{
        return LifePlayer;
}

int Player::getScore()const
{
        return Score;
}

int Player::getSpeedPlayer()const
{
        return SpeedPlayer;
}

int Player::getDamagePlayer()const
{
        return DamagePlayer;
}

Hitbox Player::getHitboxPlayer()const
{
        return HitboxPlayer;
}

Hitbox Player::getHitboxLaser()const
{
        return HitboxLaser;
}

//OTHER FUNCTIONS

void Player::decreaseLife()
{
        LifePlayer-=1;
}

bool Player::isAlive()
{
    if(LifePlayer == 0)
        return false;
    else
        return true;
}

void Player::gauche () {
     HitboxPlayer.setPositionX(HitboxPlayer.getPositionX()-1);
}

void Player::droite () {
     HitboxPlayer.setPositionX(HitboxPlayer.getPositionX()+1);
}

void Player::haut () {
     HitboxPlayer.setPositionY(HitboxPlayer.getPositionY()+1);
}

void Player::bas() {
    HitboxPlayer.setPositionY(HitboxPlayer.getPositionY()-1);
}


bool Player::isKill(EnemiesInGame & tabE, BulletsInGame & tabB){
    int px= getHitboxPlayer().getPositionX();
    int py= getHitboxPlayer().getPositionY();
    int r= getHitboxPlayer().getRadius();
    return (tabE.isHere(px,py,r) || tabB.isHere(px,py,r));

}

void Player::updatePlayer(EnemiesInGame & tabE, BulletsInGame & tabB){
   if(isKill(tabE,tabB)){
        if(LifePlayer>0){
        //Sleep(10);
            decreaseLife();
        }
        
    }
    
}

void Player::increaseTick(){
    tick++;
}

void Player::increaseScore(){
    Score++;
}

int Player::getTick(){
    return tick;
}
