/**
 \file sdlGame.h
 \brief Permet affichage en sdl

 \version 1.0
 \date 2019/02/13
 \author
*/
#ifndef _SDLGAME_H
#define _SDLGAME_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include "Game.h"

//! \brief Pour g�rer le chargement d'une image en sdl2
class Image {

private:

    SDL_Surface * surface;
    SDL_Texture * texture;
    bool has_changed;

public:
     /**
    \brief Constructeur de la classe Image de sdl
    */
    Image () ;

     /**
    \brief charge une image depuis une fichier
    */
    void loadFromFile (const char* filename, SDL_Renderer * renderer);

     /**
    \brief Permet de creer une nouvelle texture a partir du renderer en param�tre
    */
    void loadFromCurrentSurface (SDL_Renderer * renderer);

     /**
    \brief affiche sur l'�cran l'image de position x, y et de largeur  w hauteur h
    */
    void draw (SDL_Renderer * renderer, int x, int y, int w=-1, int h=-1);

     /**
    \brief retourne une texture
    */
    SDL_Texture * getTexture() const;

     /**
    \brief retourne une surface 
    */
    SDL_Surface * getSurface() const;

     /**
    \brief modifie une surface
    */
    void setSurface(SDL_Surface * surf);
};


//! \brief Pour g�rer l'affichage du jeu en SDL2
class sdlGame {

private :

    Game game;

    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Surface* tileset;
    SDL_Texture * texture;
    SDL_Rect Rect_dest;
    SDL_Rect Rect_source;

    SDL_Event event;
    TTF_Font * font;

    //A ecrit sur font
    Image font_im;
    Image over;
    Image Life;
    Image Score;
    Image font_menus[3];

    //Color
    SDL_Color menu_color[2];
    SDL_Color font_color;

    //Musique et son
    Mix_Music *musique;
    Mix_Chunk * sound;

    bool withSound;
    bool menu;
    bool bouge;

    //Image issu de png ou jpg
    Image im_menu;
    Image im_player;
    Image im_laser;
    Image im_bullet;
    Image im_bulletP;
    Image im_enemy;


public :
    /**
    \brief entier mettant a jour le scrolling
    */
    int xscroll,yscroll;

     /**
    \brief  Constructeur de la classe
    */
     sdlGame ();

     /**
    \brief  Destructeur de la classe
    */
    ~sdlGame ();

     /**
    \brief affichage du terrain grace � un syst�me de tuile(tileset)
    */
    void terAff (SDL_Renderer* renderer,SDL_Texture * texture);
     /**
    \brief Gere la boucle de jeu
    */
    void sdlBoucle ();
     /**
    \brief Affiche des sprites en sdl
    */
    void sdlAff ();

};
#endif
