#include <stdlib.h>
#include"Enemy.h"
#include"Bullet.h"
#include"Hitbox.h"
#include"Position.h"
#include<iostream>
#include<unistd.h>
#include<math.h>
using namespace std;

Enemy::Enemy()
{
      Position p(25,0);
      hitboxEnemy.setPosition(p);
      lifeEnemy = 1;
      directionEnemy = 0;
      tick=0;
      patternEnemy=2;
}

Enemy::Enemy(Hitbox & h ,int life,int pattern, bool direction)
{
       hitboxEnemy.setRadius(h.getRadius());
       hitboxEnemy.setLaser(h.getLaser());
       Position p =h.getPosition();
       hitboxEnemy.setPosition(p);
       hitboxEnemy=h;
       lifeEnemy=life;
       patternEnemy=pattern;
       directionEnemy=direction;
       tick=0;
}

Enemy::~Enemy()
{}

bool Enemy::operator ==(const Enemy & e)const{
    if (hitboxEnemy==e.hitboxEnemy){
          if (lifeEnemy==e.lifeEnemy){
            if (patternEnemy==e.patternEnemy){
                  return true;
            }
            else {
                  return false;
            }
          }
          else {
               return false;
          }
    }
    else {
      return false;
      }
}

void Enemy::setHitboxEnemy(Hitbox & h)
{
      hitboxEnemy.setRadius(h.getRadius());
      hitboxEnemy.setLaser(h.getLaser());
      hitboxEnemy.setPositionX(h.getPositionX());
      hitboxEnemy.setPositionY(h.getPositionY());
}

void Enemy::setLifeEnemy(int l)
{
	lifeEnemy=l;
}

void Enemy::setPatternEnemy(int pattern)
{
        patternEnemy=pattern;
}

void Enemy::setDirectionEnemy(bool direction)
{
      directionEnemy = direction;
}

bool Enemy::getDirectionEnemy()const
{
      return directionEnemy;
}

Hitbox Enemy::getHitboxEnemy()const
{
      return hitboxEnemy;
}

int Enemy::getLifeEnemy()const
{
	return lifeEnemy;
}

int Enemy::getPatternEnemy()const
{
   return patternEnemy;
}


void Enemy::decreaseLife()
{
    lifeEnemy --;
}


bool Enemy::isAlive()
{
	return (lifeEnemy!=0);
}

void Enemy::bougeAuto(int dimX, int dimY) 
{

      Position p(hitboxEnemy.getPositionX(),hitboxEnemy.getPositionY()+1);
      int t[9]={-4,-3,-2,-1,0,1,2,3,4};
      int dir=rand()%9;
      int x=rand()%dimX;
      if ( p.isOutOfScreen(dimX,dimY)) 
      {
            hitboxEnemy.setPositionX(x);
            hitboxEnemy.setPositionY(0);
      }
      
      else 
      {
            x=hitboxEnemy.getPositionX()+t[dir]; 
            hitboxEnemy.setPositionX(x);
            hitboxEnemy.setPositionY(hitboxEnemy.getPositionY()+1);
      } 
}

void Enemy::bougeDroit(int dimX, int dimY) 
{
      
      int newX;
      int newY;
      if(directionEnemy == 0)
      {
            newY = hitboxEnemy.getPositionY() + 1;
            newX = hitboxEnemy.getPositionX();
      }
      if(directionEnemy == 1)
      {
            newY = hitboxEnemy.getPositionY() - 1;
            newX = hitboxEnemy.getPositionX();
      }

      Position p(newX,newY);
      if( p.isOutOfScreen(dimX,dimY) )
      {
            if(hitboxEnemy.getPositionY() <= 0)
            {
                  directionEnemy = 0;
            }
            else
            {
                  if(hitboxEnemy.getPositionY() >= dimY)
                  {
                        directionEnemy = 1;
                  }
            }
      }
      else
      {
            hitboxEnemy.setPositionX(newX);
            hitboxEnemy.setPositionY(newY);
      }
}

void Enemy::bougeDiagonaleDroite (int dimX, int dimY)
{
      int newX;
      int newY;
      if(directionEnemy == 0)
      {
            newY = hitboxEnemy.getPositionY() + 1;
            newX = hitboxEnemy.getPositionX() + 1;
      }
      if(directionEnemy == 1)
      {
            newY = hitboxEnemy.getPositionY() - 1;
            newX = hitboxEnemy.getPositionX() - 1;
      }

      Position p(newX,newY);
      if( p.isOutOfScreen(dimX,dimY) )
      {
            if(hitboxEnemy.getPositionY() <= 0)
            {
                  directionEnemy = 0;
            }
            else
            {
                  if(hitboxEnemy.getPositionY() >= dimY)
                  {
                        directionEnemy = 1;
                  }
            }
      }
      else
      {
            hitboxEnemy.setPositionX(newX);
            hitboxEnemy.setPositionY(newY);
      }
}

void Enemy::bougeDiagonaleGauche (int dimX, int dimY)
{
      int newX;
      int newY;
      if(directionEnemy == 0)
      {
            newY = hitboxEnemy.getPositionY() + 1;
            newX = hitboxEnemy.getPositionX() - 1;
      }
      if(directionEnemy == 1)
      {
            newY = hitboxEnemy.getPositionY() - 1;
            newX = hitboxEnemy.getPositionX() + 1;
      }

      Position p(newX,newY);
      if( p.isOutOfScreen(dimX,dimY) )
      {
            if(hitboxEnemy.getPositionY() <= 0)
            {
                  directionEnemy = 0;
            }
            else
            {
                  if(hitboxEnemy.getPositionY() >= dimY)
                  {
                        directionEnemy = 1;
                  }
            }
      }
      else
      {
            hitboxEnemy.setPositionX(newX);
            hitboxEnemy.setPositionY(newY);
      }
}

void Enemy::bougeTriangle (int dimX, int dimY)
{
      int compteEtapes = 0;
      //int originalX = hitboxEnemy.getPositionX();
     // int originalY = hitboxEnemy.getPositionY();
      while (compteEtapes == 0)
      {
            if(hitboxEnemy.getPositionY() <= dimY +1)
            {
                  compteEtapes = 1;
            }
            else
            {
                  Enemy::bougeDiagonaleGauche(dimX,dimY);
            }
      }
      while (compteEtapes == 1)
      {
            if(hitboxEnemy.getPositionY() <= dimY +1)
            {
                  compteEtapes = 2;
            }
            else
            {
                  Enemy::bougeDiagonaleGauche(dimX,dimY);
            }
      }
}

void Enemy::operator =(const Enemy & e){
    hitboxEnemy=e.hitboxEnemy;
    lifeEnemy=e.lifeEnemy;
    patternEnemy=e.patternEnemy;
}

void Enemy::setXYEnemy(int x, int y){
    hitboxEnemy.setXYHitbox(x,y);
}

bool Enemy::isDead(int width, int posX){
   return (hitboxEnemy.getXHitbox()< posX-(width/2) && hitboxEnemy.getXHitbox()>posX+(width/2));
}

bool Enemy::isOut(int dimx , int dimy)
{
    return (hitboxEnemy.getXHitbox()<0 || hitboxEnemy.getXHitbox()>dimx || hitboxEnemy.getYHitbox()<0 || hitboxEnemy.getYHitbox()>dimy);
}

int Enemy::getTick()
{
    return tick;
}

void Enemy::setTick(int i)
{
    tick=i;
}

int Enemy::getPoints()
{
    return points;
}

