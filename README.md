# TouTou Project (LIFAP4 project) #

# Developpeurs #
SAH Crystalor, numero etudiant : 11811469
FAVRE Victor, numero etudiant : 11806797
DE ROZARIO Romane, numero etudiant : 11607306


# Description breve du projet #
TouTou Project est un jeu de type "shoot them up". Le joueur peut déplacer son personnage qui tire continuellement des balles sur un fond défilant verticalement.
Des ennemis apparaissent régulièrement au cours du défilement du fond qui tirent, comme le joueur un flot continu de balles dont la trajectoire est variable.
Chaque balle ennemie touchant le joueur lui fait perdre une vie, et chaque balle tiree par le joueur fait perdre une vie a un ennemi touche.
Des que la vie d'un ennemi tombe a 0, il disparait de l'ecran. Il peut egalement disparaitre lorsqu'ils sortent de l'ecran.
La partie s'arrete lorsque la vie du joueur tombe a 0. Tout au long de la partie, le score du joueur correspondant au nomhre d'ennemis tues s'affiche a l'ecran.
Le but du jeu est d'obtenir durant la partie le score le plus eleve possible.


# Differentes versions du projet #
Il existe deux versions fonctionnelles du projet. L'une tourne en affichage txt, l'autre avec la bibliotheque SDL2.
Une seule archive est présente dans le dossier et nécessite de compiler pour ensuite aller cherche l'executable maintxt pour jouer en txt et mainsdl pour jouer avec un afichage graphique .


# Comment jouer en SDL ? #
- Lancement du jeu :
Ouvrir l'archive TouTou Project SDL.
Ouvrir un terminal dans le dossier du jeu et lancer l'executable avec la commande : ./bin/mainsdl

- Commandes :
Un menu d'accueil s'affiche. Pour commencer ou continuer une partie, presser la touche "P".
Pour revenir au menu pendant une partie en cours, presser "M".
Pour quitter, appuyer sur "Echap" ou fermer la fenêtre.

-Deplacement du joueur :
Le joueur peut etre deplace a l'aide de la souris en maintenant le bouton gauche enfonce et deposant le personnage ou l'on veut le placer.
Il peut egalement etre deplace avec les touches directionnelles du clavier.


# Comment jouer en TXT ? #
- Lancement du jeu :
Ouvrir l'archive TouTou Project TXT.
Ouvrir un terminal dans le dossier du jeu et lancer l'executable avec la commande : ./bin/maintxt

- Deplacement du joueur :
haut : touche "O"
bas : touche "L"
gauche : touche "K"
droite : touche "M"


# Compilation #
Ouvrir un terminal dans le dossier du jeu et compiler avec la commande : make


# Organisation des dossiers #
Dans le dossier principal se situent :
- Le dossier .git qui contient les fichiers relatifs au lien avec la forge de Lyon 1.
- Le dossier bin contient les executables (maintest, maintxt, mainsdl).
- Le dossier data contient les images.
- Le dossier doc contient la documentation generee avec Doxygen (toutou.doxy qui est le fichier de configuration de doxygen et html qui contient la documentation).
- Le dossier obj contient les fichiers .o.
- Le dossier src contient les fichiers source avec les differentes classes et les main.
- A la racine, se trouvent egalement le Makefile et le fichier README.md


# Librairies utilisees #
Les librairies utilisées pour le projet sont SDL, math.h, valgrind, time.h, iostream.
